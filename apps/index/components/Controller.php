<?php
namespace index\components;

use user\UserModule;

abstract class Controller extends \core\components\Controller
{
    public $layout = '//layouts/main/user';
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow',
                'roles' => array(UserModule::ROLE_USER),
                'verbs' => array('GET'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
}