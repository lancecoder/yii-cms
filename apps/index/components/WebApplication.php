<?php
namespace index\components;

class WebApplication extends \core\components\WebApplication
{
    /**
     * Items for main menu widget can be use in layout
     * @return array
     */
    public function getMenuItems($recursive = false)
    {
        return array(
            array(
                'label' => \Yii::t('user.account', 'Account'),
                'url' => array('/user/account/view')
            ),
            array(
                'label' => \Yii::t('user.profile', 'Profile'),
                'url' => array('/user/profile/view')
            ),
            array(
                'label' => \Yii::t('user.refer', 'Refer'),
                'url' => array('/user/refer/view'),
                'visible' => !\Yii::app()->getUser()->getIsGuest() && \Yii::app()->getUser()->getModel()->getProfile(),
            ),
            array(
                'label' => \Yii::t('user.account', 'Logout') . ' [' . \Yii::app()->getUser()->getName() . ']',
                'url' => array('/user/account/logout'),
                'visible' => !\Yii::app()->getUser()->getIsGuest()
            ),
        );
    }
}