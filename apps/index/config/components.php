<?php
return array(
    'user' => array(
        'loginUrl' => array('/user/account/login'),
        'allowAutoLogin' => true,
    ),
    'bootstrap' => array(
        'class' => 'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
    ),
    'urlManager' => array(
        'rules' => array(
            //LC_APP_ID.'/friendship/<controller:\w+>/<action:\w+>'=>'user/friendship/<controller>/<action>',
            //LC_APP_ID.'/friendship/<controller:\w+>/<action:\w+>/<id:\d+>'=>'user/friendship/<controller>/<action>',
            '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            '<controller:\w+>' => '<controller>',
            '<module:[\w\/]+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
            '<module:[\w\/]+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
        ),
    ),
);