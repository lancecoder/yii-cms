<?php
return array(
    'user' => array(
        'import' => array(
            'index.modules.user.forms.*',
        ),
        'layout' => '//layouts/column1',
        'defaultController' => 'account',
        'roleLogin' => array(\user\UserModule::ROLE_USER),
    ),
);