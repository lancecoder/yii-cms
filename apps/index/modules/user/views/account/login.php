<?php
/* @var $this \index\modules\user\controllers\AccountController */
/* @var $model UserAccount */
/* @var $form TbActiveForm */
?>
<?php $form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    array(
        'enableAjaxValidation' => false,
    )
); ?>
    <fieldset>
        <div class="row">
            <?php echo $form->labelEx($model, 'username'); ?>
            <?php echo $form->textField($model, 'username'); ?>
        </div>
        <div class="row">
            <?php echo $form->labelEx($model, 'password'); ?>
            <?php echo $form->passwordField($model, 'password'); ?>
        </div>
        <div class="button-column row">
            <?php
            $this->widget('bootstrap.widgets.TbButton', array(
                'type' => 'success',
                'size' => 'small',
                'buttonType' => 'submit',
                'label' => Yii::t('user.account', 'Login'),
            ));
            ?>
        </div>
        <div class="row">
            <?php $form->error($model, 'password'); ?>
        </div>
    </fieldset>
<?php $this->endWidget(); ?>