<?php
/* @var $this \index\modules\user\controllers\AccountController */
/* @var $model UserAccount */

$this->breadcrumbs = array(
    Yii::t('user.account', 'Accounts') => array('/user/account/index'),
    Yii::t('user.account', 'Account #{id}', array('{id}' => $model->id)),
);
?>

<!--<h1><?/*=\Yii::t('user.account', 'View #{id}', array('{id}' => $model->id))*/?></h1>-->
<h1><?= Yii::t('user.account', 'Account', array('{id}' => $model->id)) ?></h1>

<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            //'id',
            array(
                'label' => $model->getAttributeLabel('role'),
                'value' => $model->getRole(),
            ),
            'phone',
            'email',
            /*'password',*/
            array(
                'name' => 'activated',
                'value' => $model->activated ? 'да' : 'нет',
            )
            /*'time_last_active',
            'time_register',*/
        ),
        'type' => array('striped', 'bordered')
    )
); ?>
