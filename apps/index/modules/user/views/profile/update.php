<?php
/* @var $this \index\modules\user\controllers\ProfileController */
/* @var $model UserProfile */
/* @var $form TbActiveForm */
/* @var $ajaxValidate bool */
/* @var $ajaxValidateId string */
?>
<h1><?= \Yii::t('user.profile', 'Profile') ?></h1>
<div class="form">

    <?php $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        array(
            'id' => $ajaxValidateId,
            'enableAjaxValidation' => $ajaxValidate,
        )
    ); ?>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'last_name', array('class' => 'span5', 'maxlength' => 63)); ?>
    </div>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'first_name', array('class' => 'span5', 'maxlength' => 31)); ?>
    </div>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'middle_name', array('class' => 'span5', 'maxlength' => 31)); ?>
    </div>

    <div class="row buttons">
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => $model->isNewRecord ? 'primary' : 'success',
                'size' => 'large',
                'label' => Yii::t('form', $model->isNewRecord ? 'Create' : 'Save'),
                'htmlOptions' => array('class' => 'span4'),
            )
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->