<?php
/* @var $this \admin\modules\user\controllers\ReferController */
/* @var $model UserRefer */
/* @var $userList array */

$this->breadcrumbs = array(
    'User Refers' => array('index'),
    'Create',
);
?>

    <h1>Create UserRefer</h1>

<?php echo $this->renderPartial('_form', array('model' => $model, 'userList' => $userList)); ?>