<?php
/* @var $this \admin\modules\user\controllers\ReferController */
/* @var $model UserRefer */
/* @var $form TbActiveForm */
/* @var $userList array */
?>

<div class="form">

    <?php $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        array(
            'id' => 'user-refer-form',
            'enableAjaxValidation' => false,
        )
    ); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="row">
        <?php echo $form->select2Row(
            $model,
            'parentId',
            array('data' => $userList, 'class' => 'span4', 'maxlength' => 7)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'refer_code', array('class' => 'span4', 'maxlength' => 7)); ?>
    </div>

    <div class="row buttons">
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => $model->isNewRecord ? 'primary' : 'success',
                'size' => 'large',
                'label' => Yii::t('form', $model->isNewRecord ? 'Create' : 'Save'),
                'htmlOptions' => array('class' => 'span4'),
            )
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->