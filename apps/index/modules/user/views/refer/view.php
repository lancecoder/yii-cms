<?php
/* @var $this \index\modules\user\controllers\AccountController */
/* @var $model UserRefer */
/* @var $parent UserRefer */
/* @var $descendants UserRefer[] */
/* @var $descendantsDepth int */

$this->breadcrumbs = array(
    \Yii::t('user.refer', 'Refers') => array('/user/refer/index'),
    \Yii::t('user.refer', 'Refer #{id}', array('{id}' => $model->fk_user_account)),
);
?>

<!--<h1><?/*=\Yii::t('user.account', 'View #{id}', array('{id}' => $model->fk_user_account))*/?></h1>-->
<h1><?= \Yii::t('user.refer', 'Refer', array('{id}' => $model->fk_user_account)) ?></h1>
<?php
$profile = $model->getRelated('Profile');
$this->widget(
    'bootstrap.widgets.TbDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            array(
                'name' => $profile ? Yii::t('user.profile', 'First Name') . ' ' . Yii::t(
                        'user.profile',
                        'Last Name'
                    ) : Yii::t('user.refer', 'Fk User Account'),
                'value' => $profile ? $profile->first_name . ' ' . $profile->last_name : $model->fk_user_account,
            ),
            'refer_code',
        ),
        'type' => array('striped', 'bordered')
    )
); ?>

<?php
/*if (!$parent->getIsNewRecord()){
    $profile = $parent->getRelated('Profile');
    echo '<h5>'.Yii::t('user.refer', 'Parent').'</h5>';
$this->widget('bootstrap.widgets.TbDetailView', array(
    'data'=>$parent,
    'attributes'=>array(
        array(
            'name' => $profile ? Yii::t('user.profile', 'First Name') . ' ' . Yii::t('user.profile', 'Last Name') : Yii::t('user.refer', 'Fk User Account'),
            'value' => $profile ? $profile->first_name . ' ' . $profile->last_name : $parent->fk_user_account,
        ),
        'refer_code',
    ),
    'type' => array('striped', 'bordered')
));
}*/
if (count($descendants)) {
    echo '<h5>' . Yii::t('user.refer', 'Descendants') . '</h5>';
    $max = pow(2, $descendantsDepth);
    $td = 0;
    echo '<table>';
    for ($i = 2; $i <= $descendantsDepth; $i++) {
        echo '<tr>';
        $count = pow(2, $i - 1);
        $colspan = $max / $count;
        for ($j = 1; $j <= $count; $j++) {
            if ($count > 1) {
                echo "<td colspan='{$colspan}'>";
            } else {
                echo '<td>';
            }
            if (isset($descendants[$td])) {
                $profile = $descendants[$td]->getRelated('Profile');
                $this->widget(
                    'bootstrap.widgets.TbDetailView',
                    array(
                        'data' => $descendants[$td],
                        'attributes' => array(
                            array(
                                'name' => $profile ? Yii::t('user.profile', 'First Name') . ' ' . Yii::t(
                                        'user.profile',
                                        'Last Name'
                                    ) : Yii::t('user.refer', 'Fk User Account'),
                                'value' => $profile ? $profile->first_name . ' ' . $profile->last_name : $descendants[$td]->fk_user_account,
                            ),
                            'refer_code',
                        ),
                        'type' => array('striped', 'bordered')
                    )
                );
            } else {
                echo '&nbsp;';
            }
            echo '</td>';
            $td++;
        }
        echo '</tr>';
    }
    echo '</table>';
}
?>
