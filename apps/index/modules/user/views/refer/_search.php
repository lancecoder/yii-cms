<?php
/* @var $this \admin\modules\user\controllers\ReferController */
/* @var $model UserRefer */
/* @var $form CActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'CActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->label($model, 'fk_user_account'); ?>
        <?php echo $form->textField($model, 'fk_user_account'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, '_lft'); ?>
        <?php echo $form->textField($model, '_lft'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, '_rght'); ?>
        <?php echo $form->textField($model, '_rght'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, '_lvl'); ?>
        <?php echo $form->textField($model, '_lvl'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'refer_code'); ?>
        <?php echo $form->textField($model, 'refer_code', array('size' => 7, 'maxlength' => 7)); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Search'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->