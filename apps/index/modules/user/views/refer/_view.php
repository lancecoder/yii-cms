<?php
/* @var $this \admin\modules\user\controllers\ReferController */
/* @var $data UserRefer */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('fk_user_account')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->fk_user_account), array('view', 'id' => $data->fk_user_account)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('_lft')); ?>:</b>
    <?php echo CHtml::encode($data->_lft); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('_rght')); ?>:</b>
    <?php echo CHtml::encode($data->_rght); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('_lvl')); ?>:</b>
    <?php echo CHtml::encode($data->_lvl); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('refer_code')); ?>:</b>
    <?php echo CHtml::encode($data->refer_code); ?>
    <br/>


</div>