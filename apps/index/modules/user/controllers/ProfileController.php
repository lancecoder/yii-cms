<?php
namespace index\modules\user\controllers;

use user\UserModule;

class ProfileController extends \index\components\Controller
{
    public $defaultAction = 'view';

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array_merge(
            array(
                array(
                    'deny', // deny all roles except USER
                    'roles' => array(UserModule::ROLE_MODERATOR, UserModule::ROLE_ADMIN,),
                ),
                array(
                    'allow',
                    'actions' => array('update'),
                    'roles' => array(UserModule::ROLE_USER),
                    'verbs' => array('POST'),
                ),
            ),
            parent::accessRules()
        );
    }

    public $updateAjaxValidate = true;
    public $updateAjaxValidateId = 'user-profile-form';

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate()
    {
        $user = \Yii::app()->getUser();
        /* @var \UserAccount $account */
        $account = $user->getModel();
        $profile = $account->getProfile();
        if (!$profile) {
            $profile = new \UserProfile();
        }

        if ($this->updateAjaxValidate)
            $this->performAjaxValidation($profile, $this->updateAjaxValidateId);

        $request = \Yii::app()->getRequest();
        if ($request->getIsPostRequest()) {
            $profile->setAttributes($request->getPost(get_class($profile), array()));
            $profile->fk_user_account = $user->getId();
            if ($profile->save()) {
                $user->setFlash('success', \Yii::t('user.profile', 'Profile save'));
                $this->redirect('view');
            }
        }

        $this->render(
            'update',
            array(
                'model' => $profile,
                'ajaxValidate' => $this->updateAjaxValidate,
                'ajaxValidateId' => $this->updateAjaxValidateId,
            )
        );
    }

    /**
     * Displays a particular model.
     */
    public function actionView()
    {
        $user = \Yii::app()->getUser();
        /* @var \UserAccount $account */
        $account = $user->getModel();
        $profile = $account->getProfile();
        if (!$profile) {
            $user->setFlash('info', \Yii::t('user.profile', 'Create your profile'));
            $this->redirect('update');
        }
        $this->render(
            'view',
            array(
                'model' => $profile,
            )
        );
    }

    /**
     * Performs the AJAX validation.
     * @param \UserProfile $model the model to be validated
     */
    protected function performAjaxValidation(\UserProfile $model, $id)
    {
        $request = \Yii::app()->getRequest();
        $ajaxValidateId = $request->getPost('ajax');
        if ($ajaxValidateId !== null && $ajaxValidateId === $id) {
            echo \CActiveForm::validate($model);
            \Yii::app()->end();
        }
    }
}
