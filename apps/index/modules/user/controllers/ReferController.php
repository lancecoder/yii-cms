<?php
namespace index\modules\user\controllers;

class ReferController extends \index\components\Controller
{
    public $defaultAction = 'view';

    /**
     * Displays a particular model.
     */
    public function actionView()
    {
        $model = $this->loadModel(\Yii::app()->getUser()->getId());
        /* @var \NestedSetBehavior $ns */
        $ns = $model->asa('NestedSet');
        $parent = $ns->parent()->find();
        if (!$parent) {
            $parent = new \UserRefer();
        }

        $descendantsDepth = 2;
        $descendants = $ns->descendants($descendantsDepth)->findAll(array('order' => '_lvl ASC, _lft ASC'));
        $this->render(
            'view',
            array(
                'model' => $model,
                'parent' => $parent,
                'descendants' => $descendants,
                'descendantsDepth' => $descendantsDepth,
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return \UserRefer the loaded model
     * @throws \CHttpException
     */
    public function loadModel($id)
    {
        $model = \UserRefer::model()->findByPk($id);
        if ($model === null) {
            throw new \CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param \UserRefer $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-account-form') {
            echo \CActiveForm::validate($model);
            \Yii::app()->end();
        }
    }
}
