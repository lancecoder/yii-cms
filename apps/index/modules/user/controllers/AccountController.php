<?php
namespace index\modules\user\controllers;

use index\components\Controller;
use user\UserModule;

class AccountController extends Controller
{
    public $defaultAction = 'view';

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array_merge(
            array(
                array(
                    'allow', // allow all users to perform 'index' and 'view' actions
                    'actions' => array('login'),
                    'roles' => array(UserModule::ROLE_GUEST),
                    'verbs' => array('GET', 'POST'),
                ),
            ),
            parent::accessRules()
        );
    }

    public function actionLogin()
    {
        $form = new \LoginForm();
        $request = \Yii::app()->getRequest();
        if ($request->getIsPostRequest()) {
            $form->setAttributes($request->getPost(get_class($form), array()));
            if ($form->login())
                $this->redirect($request->getPost('returnUrl', \Yii::app()->getHomeUrl()));
        }
        $this->layout = '//layouts/main/guest';
        $this->render(
            'login',
            array(
                'model' => $form,
            )
        );
    }

    public function actionLogout()
    {
        \Yii::app()->getUser()->logout();
        $this->redirect(\Yii::app()->homeUrl);
    }

    public function actionView()
    {
        $this->render(
            'view',
            array(
                'model' => \Yii::app()->getUser()->getModel(),
            )
        );
    }
}