<?php /* @var $this \core\components\Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
          media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print"/>
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"/>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <style type="text/css">
        .container,
        .navbar-static-top .container,
        .navbar-fixed-top .container,
        .navbar-fixed-bottom .container {
            width: 1200px;
        }

        body {
            background-color: #98a6ba;
        }
    </style>
</head>

<body>

<div class="container" id="page">

    <div id="header">
        <?php echo CHtml::encode(Yii::app()->name); ?>
    </div>
    <!-- header -->
    <?php
    $this->widget(
        'bootstrap.widgets.TbNavbar',
        array(
            //'type'=> 'inverse',
            'brand' => Yii::t('apps', Yii::app()->name),
            //'brandUrl'=>null,
            //'brandOptions'=>array(),
            'items' => array(
                array(
                    'class' => 'bootstrap.widgets.TbButtonGroup',
                    'buttons' => Yii::app()->getMenuItems(),
                ),
            ),
            //'fixed'=>'bottom',
            //'fluid'=>true,
            //'collapse'=>true,
            //'htmlOptions'=>array(),
        )
    );
    ?>
    <?php /*$this->widget('bootstrap.widgets.TbBreadcrumbs', array(
		'links'=>$this->getBreadcrumbs(),
	)); */?><!--<!-- breadcrumbs -->
    <?php
    $this->widget('bootstrap.widgets.TbAlert');
    ?>
    <div class="container">
        <div id="content">
            <?php echo $content; ?>
        </div>
        <!-- content -->
    </div>

    <div id="footer">
        <b>&copy; <?php echo date('Y'); ?> by lancecoder.</b>
    </div>
    <!-- footer -->

</div>
<!-- page -->

</body>
</html>