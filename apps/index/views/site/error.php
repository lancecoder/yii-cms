<?php
/* @var $this \index\controllers\SiteController */
/* @var $message string translated message */
/* @var $code int */
$this->setPageTitle($code . ' - ' . $message);
?>

<h2><?= Yii::t('site', 'Error #{code}', array('{code}' => $code)) ?></h2>

<div class="error">
    <?= CHtml::encode($message) ?>
</div>