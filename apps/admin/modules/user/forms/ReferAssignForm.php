<?php
class ReferAssignForm extends \core\components\FormModel
{
    public $parent_id;
    public $children_id;
    public $children_order;

    private $_children = array();
    private $_parents = array();

    public function setParentList(array $parents)
    {
        $this->_parents = $parents;
        return $this;
    }
    public function setChildrenList(array $children)
    {
        $this->_children = $children;
        return $this;
    }

    public function getParentList()
    {
        return $this->_parents;
    }
    public function getChildrenList()
    {
        return $this->_children;
    }

    public function rules()
    {
        return array(
            array('parent_id', 'in', 'range'=>array_keys($this->_parents)),
            array('children_id', 'in', 'range'=>array_keys($this->_children)),
        );
    }

    public function assign()
    {
        if ($this->validate())
        {
            if (!UserRefer::model()->exists('fk_user_account = :id', $this->children_id))
            {
                $children = new UserRefer();
                $children->setPrimaryKey($this->children_id);
                $children->setAttribute('_order', $this->children_order);
            }
            $children = $this->loadModel($this->children_id);
            $parent = $this->loadModel($this->parent_id);
            $ns = $children->nestedSet();
            if ($parent)
            {
                if ($this->children_order)
                    return $ns->appendTo($parent);
                else
                    return $ns->prependTo($parent);
            } else {

            }
        }
        return false;
    }

    /* @return UserRefer */
    private function loadModel($id)
    {
        return UserRefer::model()->findByPk($id, array(
            'with' => array(
                'Profile' => array(
                    'alias' => 'profile',
                    'condition' => '`profile`.`fk_user_account` IS NOT NULL',
                ),
                'Account' => array(
                    'alias' => 'account',
                    'condition' => '`account`.`role` = :role',
                    'params' => array(
                        'role' => \user\UserModule::ROLE_USER,
                    ),
                ),
            ),
        ));
    }

    private function attachAccount(\UserAccount $account)
    {
        $refer = new \UserRefer();
        $refer->setPrimaryKey($account->getPrimaryKey());
        $lvl = \UserRefer::model()->find(array(
            'alias' => 'parent',
            'select' => 'MIN(_lvl) as _lvl',
            'condition' => '(SELECT COUNT(1) FROM `user_refer` `child` WHERE `child`.`_lvl` = `parent`.`_lvl` + 1 AND `child`.`_lft` BETWEEN `parent`.`_lft` AND `parent`.`_rght`) < 2',
        ))->getAttribute('_lvl');
        if ($lvl) {
            $count = \UserRefer::model()->count('_order = :order AND _lvl = :lvl', array(':order' => 0, ':lvl' => $lvl+1));
            $order = $count < pow(2, $lvl)/2 ? 0 : 1;
            /* @var \UserRefer $refer*/
            $refer->setAttribute('_order', $order);
            /* @var \UserRefer[] $refers */
            $refers = \UserRefer::model()->findAllByAttributes(array('_lvl' => $lvl), array('order'=>'_lft ASC'));
            $count = count($refers);
            for($i = 1; $i <= $count; $i++) {
                if ($i <= $count/2) {
                    if ($i%2)
                        $key = $i;
                    else
                        $key = $i + $count/2 - 1;
                } else {
                    if ($i%2)
                        $key = $i - $count/2 + 1;
                    else
                        $key = $i;
                }
                $parent = $refers[$key-1];
                if (!$parent->nestedSet()->children()->exists('_order = :order', array(':order' => $order))) {
                    $ns = $refer->nestedSet();
                    if ($order)
                        return $ns->appendTo($parent);
                    else
                        return $ns->prependTo($parent);
                }
            }
            throw new \CException('place not found');
        } else {
            //root
            $refer->setAttribute('_order', 0);
            return $refer->nestedSet()->save();
        }
    }
}