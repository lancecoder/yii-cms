<?php
namespace admin\modules\user\controllers;

use user\UserModule;

class AccountController extends \admin\components\Controller
{
    public $menuIcon='user';
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array_merge(
            parent::filters(),
            array(
                'postOnly + delete', // we only allow deletion via POST request
            )
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {

        return \CMap::mergeArray(
            array(
                array(
                    'allow', // allow all users to perform 'index' and 'view' actions
                    'actions' => array('login'),
                    'roles' => array(UserModule::ROLE_GUEST),
                    'verbs' => array('GET', 'POST'),
                ),
                array(
                    'allow',
                    'actions' => array('create', 'update', 'delete'),
                    'roles' => array(UserModule::ROLE_ADMIN),
                    'verbs' => array('POST'),
                ),
            ),
            parent::accessRules()
        );
    }

    public function actionLogin()
    {
        $form = new \LoginForm();
        $request = \Yii::app()->getRequest();
        if ($request->getIsPostRequest()) {
            $form->setAttributes($request->getPost(get_class($form), array()));
            if ($form->login())
                $this->redirect($request->getPost('returnUrl', \Yii::app()->getHomeUrl()));
        }
        $this->layout = '//layouts/guest/main';
        $this->render(
            'login',
            array(
                'model' => $form,
            )
        );
    }

    public function actionLogout()
    {
        \Yii::app()->getUser()->logout();
        $this->redirect(\Yii::app()->homeUrl);
    }

    public $idParam = 'id';
    public $idActions = array('view', 'update');

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (in_array($action->getId(), $this->idActions)) {
                $params = $this->getActionParams();
                $this->_model = $this->loadModel($params['id']);
            }
            return true;
        } else return false;
    }

    public function getMenuItems()
    {
        $menu = array(
            array(
                'label' => \Yii::t('user.account', 'Manage'),
                'icon' => 'list-alt',
                'url' => array('/user/account/admin')
            ),
            array(
                'label' => \Yii::t('user.account', 'Create'),
                'icon' => 'plus',
                'url' => array('/user/account/create')
            ),
            array(
                'label' => \Yii::t('user.account', 'List'),
                'icon' => 'th-list',
                'url' => array('/user/account/index')
            ),
        );
        if ($this->_model !== null)
            $menu = \CMap::mergeArray($menu, $this->getModelActions($this->_model));
        return $menu;
    }

    private $_model = null;

    private function getModelActions(\CActiveRecord $model)
    {
        return array(
            array(
                'label' => '',
                'icon' => '',
            ),
            array(
                'label' => get_class($model).' #'.$model->getPrimaryKey(),
                'icon' => 'file',
            ),
            array(
                'label' => \Yii::t('user.account', 'View #{id}', array('{id}' => $model->id)),
                'icon' => 'zoom-in',
                'url' => array('/user/account/view', 'id' => $model->id)
            ),
            array(
                'label' => \Yii::t('user.account', 'Update #{id}', array('{id}' => $model->id)),
                'icon' => 'edit',
                'url' => array('/user/account/update', 'id' => $model->id)
            ),
            array(
                'label' => \Yii::t('user.account', 'Delete #{id}', array('{id}' => $model->id)),
                'icon' => 'remove',
                'url' => '#',
                'linkOptions' => array(
                    'submit' => array('/user/account/delete', 'id' => $model->id),
                    'csrf' => \Yii::app()->getRequest()->enableCsrfValidation,
                    'confirm' => \Yii::t(
                        'form',
                        'Are you sure you want to delete this item #{id}?',
                        array('{id}' => $model->id)
                    ),
                )
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    public $createAjaxValidate = true;
    public $createAjaxValidateId = 'user-account-form';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new \UserAccount('insert');

        if ($this->createAjaxValidate)
            $this->performAjaxValidation($model, $this->createAjaxValidateId);

        $request = \Yii::app()->getRequest();
        if ($request->getIsPostRequest()) {
            $model->setAttributes($request->getPost(get_class($model), array()));
            if ($model->save())
                $this->redirect(array('profile/create', 'id' => $model->getPrimaryKey()));
        }

        $this->render(
            'create',
            array(
                'model' => $model,
                'roleList' => \UserAccount::getRoleLabels(),
                'ajaxValidate' => $this->createAjaxValidate,
                'ajaxValidateId' => $this->createAjaxValidateId,
            )
        );
    }

    public $updateAjaxValidate = true;
    public $updateAjaxValidateId = 'user-account-form';

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if ($this->updateAjaxValidate)
            $this->performAjaxValidation($model, $this->updateAjaxValidateId);

        $request = \Yii::app()->getRequest();
        if ($request->getIsPostRequest()) {
            $model->setAttributes($request->getPost(get_class($model), array()));
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->getPrimaryKey()));
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'roleList' => \UserAccount::getRoleLabels(),
                'ajaxValidate' => $this->updateAjaxValidate,
                'ajaxValidateId' => $this->updateAjaxValidateId,
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        $request = \Yii::app()->getRequest();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if ($request->getQuery('ajax') === null)
            $this->redirect($request->getPost('returnUrl', \Yii::app()->getHomeUrl()));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new \CActiveDataProvider('UserAccount');
        $this->render(
            'index',
            array(
                'dataProvider' => $dataProvider,
            )
        );
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new \UserAccount('search');
        $model->unsetAttributes(); // clear any default values
        $request = \Yii::app()->getRequest();
        $attributes = $request->getQuery('UserAccount');
        if ($attributes !== null)
            $model->setAttributes($attributes);

        $this->render(
            'admin',
            array(
                'model' => $model,
                'roleList' => array_merge(array('' => ''), \UserAccount::getRoleLabels()),
                'activatedList' => $activatedList = array('' => '', 0 => 'Не активные', 1 => 'Активные'),
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return \UserAccount the loaded model
     * @throws \CHttpException
     */
    public function loadModel($id)
    {
        $model = \UserAccount::model()->findByPk($id);
        if ($model === null)
            throw new \CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param \UserAccount $model the model to be validated
     */
    protected function performAjaxValidation(\UserAccount $model, $id)
    {
        $request = \Yii::app()->getRequest();
        $ajaxValidateId = $request->getPost('ajax');
        if ($ajaxValidateId !== null && $ajaxValidateId === $id) {
            echo \CActiveForm::validate($model);
            \Yii::app()->end();
        }
    }
}
