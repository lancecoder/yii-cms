<?php
namespace admin\modules\user\controllers;

use core\components\FormModel;
use user\UserModule;

class ReferController extends \admin\components\Controller
{
    public $defaultAction = 'create';

    //public function actionDefine($id)
    public function testData()
    {
        foreach(\UserAccount::model()->findAll() as $model)
            $this->attachAccount($model);

        for ($i=1; $i<=100; $i++) {
            $user = new \UserAccount();
            $user->role=UserModule::ROLE_USER;
            $rand = intval(rand(0,1000000));
            $user->phone = $rand;
            $user->password = $rand;
            $user->activated = 1;
            $user->time_last_active = new \CDbExpression('NOW()');
            $user->save();

            $profile = new \UserProfile();
            $profile->setPrimaryKey($user->getPrimaryKey());

            $profile->first_name=$user->getPrimaryKey();
            $profile->last_name = $user->getPrimaryKey();
            $profile->middle_name = $user->getPrimaryKey();
            $profile->save();

            $this->attachAccount($user);
        }
    }

    public function actionView($id)
    {
        //$this->testData();return;
        $with = array(
            'Profile' => array(
                'alias' => 'profile',
                'condition' => '`profile`.`fk_user_account` IS NOT NULL',
            ),
            'Account' => array(
                'alias' => 'account',
                'condition' => '`account`.`role` = :role',
                'params' => array(
                    'role' => UserModule::ROLE_USER,
                ),
            ),
        );
        //$model = \UserRefer::model()->with($with)->findByPk($id);
        $model = $this->loadModel($id, array('with' => $with));
        if (!$model)
            $this->modelNotFound();
        $this->render('view', array(
            'model' => $model,
            'condition' => array(
                'order' => '_order ASC',
                'with' => $with,
            ),
        ));
    }

    private function attachAccount(\UserAccount $account)
    {
        $refer = new \UserRefer();
        $refer->setPrimaryKey($account->getPrimaryKey());
        $lvl = \UserRefer::model()->find(array(
            'alias' => 'parent',
            'select' => 'MIN(_lvl) as _lvl',
            'condition' => '(SELECT COUNT(1) FROM `user_refer` `child` WHERE `child`.`_lvl` = `parent`.`_lvl` + 1 AND `child`.`_lft` BETWEEN `parent`.`_lft` AND `parent`.`_rght`) < 2',
        ))->getAttribute('_lvl');
        if ($lvl) {
            $count = \UserRefer::model()->count('_order = :order AND _lvl = :lvl', array(':order' => 0, ':lvl' => $lvl+1));
            $order = $count < pow(2, $lvl)/2 ? 0 : 1;
            /* @var \UserRefer $refer*/
            $refer->setAttribute('_order', $order);
            /* @var \UserRefer[] $refers */
            $refers = \UserRefer::model()->findAllByAttributes(array('_lvl' => $lvl), array('order'=>'_lft ASC'));
            $count = count($refers);
            for($i = 1; $i <= $count; $i++) {
                if ($i <= $count/2) {
                    if ($i%2)
                        $key = $i;
                    else
                        $key = $i + $count/2 - 1;
                } else {
                    if ($i%2)
                        $key = $i - $count/2 + 1;
                    else
                        $key = $i;
                }
                $parent = $refers[$key-1];
                if (!$parent->nestedSet()->children()->exists('_order = :order', array(':order' => $order))) {
                    $ns = $refer->nestedSet();
                    if ($order)
                        return $ns->appendTo($parent);
                    else
                        return $ns->prependTo($parent);
                }
            }
            throw new \CException('place not found');
        } else {
            //root
            $refer->setAttribute('_order', 0);
            return $refer->nestedSet()->save();
        }
    }

    public function actionCreate()
    {
        $with = array(
            'Profile' => array(
                'alias' => 'profile',
                'condition' => '`profile`.`fk_user_account` IS NOT NULL',
            ),
            'Account' => array(
                'alias' => 'account',
                'condition' => '`account`.`role` = :role',
                'params' => array(
                    'role' => UserModule::ROLE_USER,
                ),
            ),
        );
        //$model = \UserRefer::model()->with($with)->findByPk($id);
        $model = $this->loadModel($id, array('with' => $with));
        if (!$model)
            $this->modelNotFound();
        $this->render('view', array(
            'model' => $model,
            'condition' => array(
                'order' => '_order ASC',
                'with' => $with,
            ),
        ));
    }
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return \UserRefer the loaded model
     * @throws \CHttpException
     */
    public function loadModel($id, $condition = '', $param = array())
    {
        $model = \UserRefer::model()->findByPk($id, $condition, $param);
        if ($model === null)
            throw new \CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param \UserRefer $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-account-form') {
            echo \CActiveForm::validate($model);
            \Yii::app()->end();
        }
    }
}