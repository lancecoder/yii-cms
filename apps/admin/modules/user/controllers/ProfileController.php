<?php
namespace admin\modules\user\controllers;

use user\UserModule;

class ProfileController extends \admin\components\Controller
{
    public $menuIcon='briefcase';
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array_merge(
            parent::filters(),
            array(
                'postOnly + delete', // we only allow deletion via POST request
            )
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {

        return \CMap::mergeArray(
            array(
                array(
                    'allow',
                    'actions' => array('create', 'update', 'delete'),
                    'roles' => array(UserModule::ROLE_ADMIN),
                    'verbs' => array('POST'),
                ),
            ),
            parent::accessRules()
        );
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (in_array($action->getId(), array('view', 'update'))) {
                $params = $this->getActionParams();
                $this->_model = $this->loadModel($params['id']);
            }
            return true;
        } else {
            return false;
        }
    }

    public function getMenuItems()
    {
        $menu = array(
            array(
                'label' => \Yii::t('user.profile', 'Manage'),
                'url' => array('/user/profile/admin')
            ),
            array(
                'label' => \Yii::t('user.profile', 'Create'),
                'url' => array('/user/profile/create')
            ),
            array(
                'label' => \Yii::t('user.profile', 'List'),
                'url' => array('/user/profile/index')
            ),
        );
        if ($this->_model !== null)
            $menu = \CMap::mergeArray($menu, $this->getModelActions($this->_model));
        return $menu;
    }

    private $_model = null;

    private function getModelActions($model)
    {
        return array(
            array(
                'label' => \Yii::t('user.profile', 'View #{id}', array('{id}' => $model->fk_user_account)),
                'url' => array('/user/profile/view', 'id' => $model->fk_user_account)
            ),
            array(
                'label' => \Yii::t('user.profile', 'Update #{id}', array('{id}' => $model->fk_user_account)),
                'url' => array('/user/profile/update', 'id' => $model->fk_user_account)
            ),
            array(
                'label' => \Yii::t('user.profile', 'Delete #{id}', array('{id}' => $model->fk_user_account)),
                'url' => '#',
                'linkOptions' => array(
                    'submit' => array('/user/profile/delete', 'id' => $model->fk_user_account),
                    'csrf' => \Yii::app()->getRequest()->enableCsrfValidation,
                    'confirm' => \Yii::t(
                        'form',
                        'Are you sure you want to delete this item #{id}?',
                        array('{id}' => $model->fk_user_account)
                    ),
                )
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render(
            'view',
            array(
                'model' => $this->loadModel($id),
            )
        );
    }

    public $createAjaxValidate = true;
    public $createAjaxValidateId = 'user-account-form';

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($id = null)
    {
        $model = new \UserProfile('insert');
        if ($id)
            $model->setPrimaryKey($id);

        // Uncomment the following line if AJAX validation is needed
        if ($this->createAjaxValidate)
            $this->performAjaxValidation($model, $this->createAjaxValidateId);

        $request = \Yii::app()->getRequest();
        if ($request->getIsPostRequest()) {
            $model->setAttributes($request->getPost(get_class($model), array()));
            if ($model->save())
                $this->redirect(array('refer/create', 'id' => $model->getPrimaryKey()));
        }
        $userList = $this->getAllowedUserList();
        if ($userList === array()) {
            \Yii::app()->getUser()->setFlash('info', \Yii::t('user.profile', 'All account have profile.'));
            $this->redirect(array('account/create'));
        }

        $this->render(
            'create',
            array(
                'model' => $model,
                'userList' => $userList,
                'ajaxValidate' => $this->createAjaxValidate,
                'ajaxValidateId' => $this->createAjaxValidateId,
            )
        );
    }

    public $updateAjaxValidate = true;
    public $updateAjaxValidateId = 'user-account-form';

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        if ($this->updateAjaxValidate)
            $this->performAjaxValidation($model, $this->updateAjaxValidateId);

        $request = \Yii::app()->getRequest();
        if ($request->getIsPostRequest()) {
            $model->setAttributes($request->getPost(get_class($model), array()));
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->getPrimaryKey()));
        }

        $this->render(
            'update',
            array(
                'model' => $model,
                'userList' => $this->getAllowedUserList($model),
                'ajaxValidate' => $this->updateAjaxValidate,
                'ajaxValidateId' => $this->updateAjaxValidateId,
            )
        );
    }

    private function getAllowedUserList(\UserProfile $withModel = null)
    {
        $condition = 'p.fk_user_account IS NULL';
        if ($withModel !== null)
            $condition .= ' OR p.fk_user_account = ' . $withModel->fk_user_account;
        $users = \UserAccount::model()->with(
            array(
                'Profile' => array(
                    'alias' => 'p',
                    'condition' => $condition,
                )
            )
        )->findAll();
        $module = $this->getModule();
        return \CMap::mergeArray(
            array('' => ''),
            \CHtml::listData(
                $users,
                'id',
                function (\UserAccount $data) use ($module) {
                    /* @var UserModule $module */
                    if ($module->phoneLogin && $module->emailLogin)
                        return "$data->email ($data->phone)";
                    elseif ($module->phoneLogin)
                        return $data->phone;
                    elseif ($module->emailLogin)
                        return $data->email;
                }
            )
        );
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        $request = \Yii::app()->getRequest();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if ($request->getQuery('ajax') === null)
            $this->redirect($request->getPost('returnUrl', \Yii::app()->getHomeUrl()));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider = new \CActiveDataProvider('UserProfile');
        $this->render(
            'index',
            array(
                'dataProvider' => $dataProvider,
            )
        );
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new \UserProfile('search');
        $model->unsetAttributes(); // clear any default values
        $request = \Yii::app()->getRequest();
        $attributes = $request->getQuery('UserProfile');
        if ($attributes !== null)
            $model->setAttributes($attributes);

        $this->render(
            'admin',
            array(
                'model' => $model,
            )
        );
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return \UserProfile the loaded model
     * @throws \CHttpException
     */
    public function loadModel($id)
    {
        $model = \UserProfile::model()->findByPk($id);
        if ($model === null)
            throw new \CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param \UserProfile $model the model to be validated
     */
    protected function performAjaxValidation($model, $id)
    {
        $request = \Yii::app()->getRequest();
        $ajaxValidateId = $request->getPost('ajax');
        if ($ajaxValidateId !== null && $ajaxValidateId === $id) {
            echo \CActiveForm::validate($model);
            \Yii::app()->end();
        }
    }
}
