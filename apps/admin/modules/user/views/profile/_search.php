<?php
/* @var $this \admin\modules\user\controllers\ProfileController */
/* @var $model UserProfile */
/* @var $form TbActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'fk_user_account', array('class' => 'span5')); ?>
    </div>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'first_name', array('class' => 'span5', 'maxlength' => 31)); ?>
    </div>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'middle_name', array('class' => 'span5', 'maxlength' => 31)); ?>
    </div>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'last_name', array('class' => 'span5', 'maxlength' => 63)); ?>
    </div>

    <div class="row buttons">
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'info',
                'size' => 'large',
                'label' => Yii::t('form', 'Search'),
                'htmlOptions' => array('class' => 'span4'),
            )
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->