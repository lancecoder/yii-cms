<?php
/* @var $this \admin\modules\user\controllers\ProfileController */
/* @var $model UserProfile */

$this->breadcrumbs = array(
    Yii::t('user.profile', 'Profiles') => array('/user/profile/index'),
    Yii::t('user.profile', 'Profile #{id}', array('{id}' => $model->fk_user_account)),
);
?>
<h1><?= Yii::t('user.profile', 'View #{id}', array('{id}' => $model->fk_user_account)) ?></h1>

<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'fk_user_account',
            'last_name',
            'first_name',
            'middle_name',
        ),
        'type' => array('striped', 'bordered')
    )
); ?>
