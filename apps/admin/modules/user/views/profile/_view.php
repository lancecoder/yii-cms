<?php
/* @var $this \admin\modules\user\controllers\ProfileController */
/* @var $data UserProfile */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('fk_user_account')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->fk_user_account), array('view', 'id' => $data->fk_user_account)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
    <?php echo CHtml::encode($data->first_name); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('middle_name')); ?>:</b>
    <?php echo CHtml::encode($data->middle_name); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b>
    <?php echo CHtml::encode($data->last_name); ?>
    <br/>


</div>