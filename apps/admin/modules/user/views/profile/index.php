<?php
/* @var $this \admin\modules\user\controllers\ProfileController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    Yii::t('user.profile', 'Profiles'),
    Yii::t('user.profile', 'List'),
);
?>

<h1><?= Yii::t('user.profile', 'List') ?></h1>

<?php $this->widget(
    'bootstrap.widgets.TbListView',
    array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    )
); ?>
