<?php
/* @var $this \admin\modules\user\controllers\ProfileController */
/* @var $model UserProfile */

$this->breadcrumbs = array(
    Yii::t('user.profile', 'Profiles') => array('/user/profile/index'),
    Yii::t('user.profile', 'Manage'),
);

Yii::app()->clientScript->registerScript(
    'search',
    "
   $('.search-button').click(function(){
       $('.search-form').toggle();
       return false;
   });
   $('.search-form form').submit(function(){
       $('#user-profile-grid').yiiGridView('update', {
           data: $(this).serialize()
       });
       return false;
   });
   "
);
?>

<h1><?= Yii::t('user.profile', 'Manage') ?></h1>

<p>
    <?=
    Yii::t(
        'form',
        'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
       or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'
    )?>
</p>

<?php echo CHtml::link(Yii::t('form', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
        '_search',
        array(
            'model' => $model,
        )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
    'bootstrap.widgets.TbGridView',
    array(
        'id' => 'user-profile-grid',
        'type' => array('striped', 'condensed'),
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'fk_user_account',
                'htmlOptions' => array('width' => '45px'),
            ),
            'first_name',
            'middle_name',
            'last_name',
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
            ),
        ),
    )
); ?>
