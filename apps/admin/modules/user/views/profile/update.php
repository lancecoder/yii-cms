<?php
/* @var $this \admin\modules\user\controllers\ProfileController */
/* @var $model UserProfile */
/* @var $userList array */
/* @var $ajaxValidate bool */
/* @var $ajaxValidateId string */

$this->breadcrumbs = array(
    Yii::t('user.profile', 'Profiles') => array('/user/profile/index'),
    Yii::t('user.profile', 'Profile #{id}', array('{id}' => $model->fk_user_account)) => array(
        'view',
        'id' => $model->fk_user_account
    ),
    Yii::t('user.profile', 'Update'),
);
?>
    <h1><?= Yii::t('user.profile', 'Update #{id}', array('{id}' => $model->fk_user_account)) ?></h1>
<?= $this->renderPartial('_form', array('model' => $model, 'userList' => $userList, 'ajaxValidate' => $ajaxValidate, 'ajaxValidateId' => $ajaxValidateId)); ?>