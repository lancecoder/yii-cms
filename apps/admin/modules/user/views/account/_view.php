<?php
/* @var $this \admin\modules\user\controllers\AccountController */
/* @var $data UserAccount */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('role')); ?>:</b>
    <?php echo CHtml::encode($data->getRole()); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
    <?php echo CHtml::encode($data->phone); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
    <?php echo CHtml::encode($data->email); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
    <?php echo CHtml::encode($data->password); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('activated')); ?>:</b>
    <?php echo CHtml::encode($data->activated); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('time_last_active')); ?>:</b>
    <?php echo CHtml::encode($data->time_last_active); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('time_register')); ?>:</b>
    <?php echo CHtml::encode($data->time_register); ?>
    <br/>

</div>