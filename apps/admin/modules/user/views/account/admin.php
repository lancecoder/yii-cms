<?php
/* @var $this \admin\modules\user\controllers\AccountController */
/* @var $model UserAccount */
/* @var $roleList array */
/* @var $activatedList array */

$this->breadcrumbs = array(
    Yii::t('user.account', 'Accounts') => array('/user/account/index'),
    Yii::t('user.account', 'Manage'),
);
Yii::app()->clientScript->registerScript(
    'search',
    "
   $('.search-button').click(function(){
       $('.search-form').toggle();
       return false;
   });
   $('.search-form form').submit(function(){
       $('#user-account-grid').yiiGridView('update', {
           data: $(this).serialize()
       });
       return false;
   });
   "
);
?>

<h1><?php echo Yii::t('user.account', 'Manage') ?></h1>

<p>
    <?php echo Yii::t(
        'form',
        'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
       or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.'
    )?>
</p>

<?php echo CHtml::link(Yii::t('form', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
        '_search',
        array(
            'model' => $model,
            'roleList' => $roleList,
            'activatedList' => $activatedList,
        )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
    'bootstrap.widgets.TbGridView',
    array(
        'id' => 'user-account-grid',
        'type' => array('striped', 'condensed'),
        'dataProvider' => new CActiveDataProvider($model, array('criteria'=>$model->search())),
        'filter' => $model,
        'columns' => array(
            array(
                'name' => 'id',
                'htmlOptions' => array('width' => '45px'),
            ),
            array(
                'name' => 'role',
                'value' => '$data->getRole()',
                'filter' => CHtml::activeDropDownList($model, 'role', $roleList),
            ),
            'phone',
            'email',
            'password',
            array(
                'name' => 'activated',
                'filter' => CHtml::activeDropDownList($model, 'activated', $activatedList),
            ),
            /*
            'time_last_active',
            'time_register',
            */
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
            ),
        ),
    )
); ?>
