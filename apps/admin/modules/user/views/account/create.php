<?php
/* @var $this \admin\modules\user\controllers\AccountController */
/* @var $model UserAccount */
/* @var $roleList array */
/* @var $ajaxValidate bool */
/* @var $ajaxValidateId string */

$this->breadcrumbs = array(
    Yii::t('user.account', 'Profiles') => array('/user/account/index'),
    Yii::t('user.account', 'Create'),
);
?>
    <h1><?= Yii::t('user.account', 'Create') ?></h1>
<?php echo $this->renderPartial('_form', array('model' => $model, 'roleList' => $roleList, 'ajaxValidate' => $ajaxValidate, 'ajaxValidateId' => $ajaxValidateId)); ?>