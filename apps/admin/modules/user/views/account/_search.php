<?php
/* @var $this \admin\modules\user\controllers\AccountController */
/* @var $model UserAccount */
/* @var $roleList array */
/* @var $activatedList array */
/* @var $form TbActiveForm */
?>

<div class="wide form">

    <?php $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
        )
    ); ?>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'id', array('class' => 'span5',)); ?>
    </div>

    <div class="row">
        <?php echo $form->dropDownListRow($model, 'role', $roleList, array('class' => 'span5', 'maxlength' => 6)); ?>
    </div>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'phone', array('class' => 'span5', 'maxlength' => 11)); ?>
    </div>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 127)); ?>
    </div>

    <div class="row">
        <?php echo $form->dropDownListRow($model, 'activated', $activatedList, array('class' => 'span5',)); ?>
    </div>

    <div class="row">
        <?php /*echo $form->datepickerRow($model,'time_last_active',array('class'=>'span5','template'=>'{input}')); */ ?>
        <?php
        echo $form->labelEx($model, 'time_last_active');
        $this->widget(
            'bootstrap.widgets.TbDateRangePicker',
            array(
                'form' => $form,
                'model' => $model,
                'attribute' => 'time_last_active',
                'callback' => 'js:function(start,end){return ;}',
            )
        );
        ?>
    </div>

    <div class="row">
        <?php /*echo $form->datepickerRow($model,'time_register',array('class'=>'span5',)); */ ?>
        <?php
        echo $form->labelEx($model, 'time_register');
        $this->widget(
            'bootstrap.widgets.TbDateRangePicker',
            array(
                'form' => $form,
                'model' => $model,
                'attribute' => 'time_register',
                'callback' => 'js:function(start,end){return;}',
            )
        );
        ?>
    </div>

    <div class="row buttons">
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => 'info',
                'size' => 'large',
                'label' => Yii::t('form', 'Search'),
                'htmlOptions' => array('class' => 'span4'),
            )
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->