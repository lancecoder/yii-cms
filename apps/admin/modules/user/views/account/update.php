<?php
/* @var $this \admin\modules\user\controllers\AccountController */
/* @var $model UserAccount */
/* @var $roleList array */
/* @var $ajaxValidate bool */
/* @var $ajaxValidateId string */

$this->breadcrumbs = array(
    Yii::t('user.account', 'Accounts') => array('/user/account/index'),
    Yii::t('user.account', 'Account #{id}', array('{id}' => $model->id)) => array('view', 'id' => $model->id),
    Yii::t('user.account', 'Update'),
);
?>
    <h1><?= Yii::t('user.account', 'Update #{id}', array('{id}' => $model->id)) ?></h1>
<?php echo $this->renderPartial('_form', array('model' => $model, 'roleList' => $roleList, 'ajaxValidate' => $ajaxValidate, 'ajaxValidateId' => $ajaxValidateId)); ?>