<?php
/* @var $this \admin\modules\user\controllers\AccountController */
/* @var $model UserAccount */

$this->breadcrumbs = array(
    Yii::t('user.account', 'Accounts') => array('/user/account/index'),
    Yii::t('user.account', 'Account #{id}', array('{id}' => $model->id)),
);
?>

<h1><?= Yii::t('user.account', 'View #{id}', array('{id}' => $model->id)) ?></h1>

<?php $this->widget(
    'bootstrap.widgets.TbDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'id',
            array(
                'label' => $model->getAttributeLabel('role'),
                'value' => $model->getRole(),
            ),
            'phone',
            'email',
            'password',
            'activated',
            'time_last_active',
            'time_register',
        ),
        'type' => array('striped', 'bordered')
    )
); ?>
