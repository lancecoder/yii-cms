<?php
/* @var $this \admin\modules\user\controllers\AccountController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    Yii::t('user.account', 'Accounts'),
    Yii::t('user.account', 'List'),
);
?>

<h1><?= Yii::t('user.account', 'List') ?></h1>

<?php $this->widget(
    'bootstrap.widgets.TbListView',
    array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    )
); ?>
