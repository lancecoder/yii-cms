<?php
/* @var $this \admin\modules\user\controllers\AccountController */
/* @var $model UserAccount */
/* @var $roleList array */
/* @var $form TbActiveForm */
/* @var $ajaxValidate bool */
/* @var $ajaxValidateId string */
?>

<div class="form">

    <?php $form = $this->beginWidget(
        'core\\components\\ActiveForm',
        array(
            'id' => $ajaxValidateId,
            'enableAjaxValidation' => $ajaxValidate,
        )
    ); ?>

    <p class="note"><?php echo Yii::t(
            'form',
            'Fields with {symbol} are required.',
            array('{symbol}' => '<span class="required">*</span>')
        ) ?></p>

    <div class="row">
        <?php echo $form->select2Row(
            $model,
            'role',
            array('data' => $roleList, 'class' => 'span4', 'maxlength' => 6)
        ); ?>
    </div>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'phone', array('class' => 'span5', 'maxlength' => 11)); ?>
    </div>

    <div class="row">
        <?php echo $form->textFieldRow($model, 'email', array('class' => 'span5', 'maxlength' => 127)); ?>
    </div>

    <div class="row">
        <?php echo $form->passwordFieldRow($model, 'password', array('class' => 'span5', 'maxlength' => 32)); ?>
    </div>

    <div class="row">
        <?php echo $form->toggleButtonRow($model, 'activated', array('class' => 'span5')); ?>
    </div>

    <div class="row">
        <?php echo $form->datepickerRow($model, 'time_last_active', array('class' => 'span5')); ?>
    </div>

    <div class="row">
        <?php echo $form->datepickerRow($model, 'time_register', array('class' => 'span5')); ?>
    </div>

    <div class="row buttons">
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'type' => $model->isNewRecord ? 'primary' : 'success',
                'size' => 'large',
                'label' => Yii::t('form', $model->isNewRecord ? 'Create' : 'Save'),
                'htmlOptions' => array('class' => 'span4'),
            )
        );
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->