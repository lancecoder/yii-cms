<?php
/* @var $this \admin\modules\user\controllers\ReferController */
/* @var $model UserRefer */
/* @var $userList array */

$this->breadcrumbs = array(
    'User Refers' => array('index'),
    $model->fk_user_account => array('view', 'id' => $model->fk_user_account),
    'Update',
);
?>

    <h1>Update UserRefer <?php echo $model->fk_user_account; ?></h1>

<?php echo $this->renderPartial('_form', array('model' => $model, 'userList' => $userList)); ?>