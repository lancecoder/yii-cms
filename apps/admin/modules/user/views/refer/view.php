<?php
/* @var $this \index\modules\user\controllers\AccountController */
/* @var $model UserRefer */
/* @var $condition array */

$this->breadcrumbs = array(
    \Yii::t('user.refer', 'Refers') => array('/user/refer/index'),
    \Yii::t('user.refer', 'Refer #{id}', array('{id}' => $model->getPrimaryKey())),
);
?>

<!--<h1><?/*=\Yii::t('user.account', 'View #{id}', array('{id}' => $model->fk_user_account))*/?></h1>-->
<h1><?= \Yii::t('user.refer', 'Refer', array('{id}' => $model->getPrimaryKey())) ?></h1>
<?php
$this->widget(
    'bootstrap.widgets.TbDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'Profile.first_name',
            'Profile.last_name',
            'refer_code',
        ),
        'type' => array('striped', 'bordered')
    )
);

$this->widget('user\\widgets\\ReferDetailView',
    array(
        'data' => $model,
        'attributes' => array(
            'Profile.first_name',
            'Profile.last_name',
            'refer_code'
        ),
        'condition' => $condition,
        'depth' => 2
    )
);
