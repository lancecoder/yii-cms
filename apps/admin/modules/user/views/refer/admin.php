<?php
/* @var $this \admin\modules\user\controllers\ReferController */
/* @var $model UserRefer */

$this->breadcrumbs = array(
    'User Refers' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List UserRefer', 'url' => array('index')),
    array('label' => 'Create UserRefer', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript(
    'search',
    "
   $('.search-button').click(function(){
       $('.search-form').toggle();
       return false;
   });
   $('.search-form form').submit(function(){
       $('#user-refer-grid').yiiGridView('update', {
           data: $(this).serialize()
       });
       return false;
   });
   "
);
?>

<h1>Manage User Refers</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial(
        '_search',
        array(
            'model' => $model,
        )
    ); ?>
</div><!-- search-form -->

<?php $this->widget(
    'zii.widgets.grid.CGridView',
    array(
        'id' => 'user-refer-grid',
        'dataProvider' => $model->search(),
        'filter' => $model,
        'columns' => array(
            'fk_user_account',
            '_lft',
            '_rght',
            '_lvl',
            'refer_code',
            array(
                'class' => 'CButtonColumn',
            ),
        ),
    )
); ?>
