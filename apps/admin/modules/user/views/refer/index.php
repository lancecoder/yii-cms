<?php
/* @var $this \admin\modules\user\controllers\ReferController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'User Refers',
);

$this->menu = array(
    array('label' => 'Create UserRefer', 'url' => array('create')),
    array('label' => 'Manage UserRefer', 'url' => array('admin')),
);
?>

<h1>User Refers</h1>

<?php $this->widget(
    'zii.widgets.CListView',
    array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    )
); ?>
