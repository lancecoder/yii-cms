<?php
return array(
    'user' => array(
        'loginUrl' => array('/user/account/login'),
    ),
    'bootstrap' => array(
        'class' => 'ext.bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
    ),
    'urlManager' => array(
        'rules' => array(
            //LC_APP_ID.'/friendship/<controller:\w+>/<action:\w+>'=>'user/friendship/<controller>/<action>',
            //LC_APP_ID.'/friendship/<controller:\w+>/<action:\w+>/<id:\d+>'=>'user/friendship/<controller>/<action>',
            LC_APP_ID . '/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            LC_APP_ID . '/<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            LC_APP_ID . '/<controller:\w+>' => '<controller>',
            LC_APP_ID => '',
            LC_APP_ID . '/<module:[\w\/]+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
            LC_APP_ID . '/<module:[\w\/]+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
        ),
    ),
);