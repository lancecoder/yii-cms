<?php
return array(
    'user' => array(
        'import' => array(
            'admin.modules.user.forms.*',
        ),
        'controllerMap' => array(
            'account' => array(
                'class' => 'admin\\modules\\user\\controllers\\AccountController',
            ),
            'profile' => array(
                'class' => 'admin\\modules\\user\\controllers\\ProfileController',
            ),
        ),
        'layout' => '//layouts/column2',
        'defaultController' => 'account',
        'roleLogin' => array(\user\UserModule::ROLE_ADMIN, \user\UserModule::ROLE_MODERATOR),
    ),
);