<?php
return CMap::mergeArray(
    require LC_CORE_PATH . LC_DS . 'config' . LC_DS . 'application.php',
    array(
        'defaultController' => 'user/account',
        'preload' => array('bootstrap', 'log'),
        //'language'              => 'en_us',
        'language' => 'ru',
        'modules' => require 'modules.php',
        'components' => require 'components.php',
        'params' => require 'params.php',
    )
);