<?php /* @var $this \core\components\Controller */ ?>
<?php $this->beginContent('//layouts/user/main'); ?>
    <div class="container">
        <div class="span-24">
            <div id="content">
                <?php echo $content; ?>
            </div>
            <!-- content -->
        </div>
        <div class="span-5 last">
            <div id="sidebar">
                <?php
                $this->widget(
                    'bootstrap.widgets.TbMenu',
                    array(
                        'type' => 'pills',
                        'stacked' => true,
                        'items' => $this->getMenuItems(),
                    )
                );
                ?>
            </div>
            <!-- sidebar -->
        </div>
    </div>
<?php $this->endContent(); ?>