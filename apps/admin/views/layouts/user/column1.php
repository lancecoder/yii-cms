<?php $this->beginContent('//layouts/user/main'); ?>
    <div class="container">
        <div id="content">
            <?php echo $content; ?>
        </div>
        <!-- content -->
    </div>
<?php $this->endContent(); ?>