<?php
/* @var $this \admin\controllers\SiteController */
/* @var $message string */
/* @var $code int */
?>

<h2><?= Yii::t('site', 'Error #{code}', array('{code}' => $code)) ?></h2>

<div class="error">
    <?= CHtml::encode($message) ?>
</div>