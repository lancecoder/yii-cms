<?php
namespace admin\controllers;

use admin\components\Controller;

class SiteController extends Controller
{
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return \CMap::mergeArray(
            array(
                array(
                    'allow', // allow all users to perform 'index' and 'view' actions
                    'actions' => array('error'),
                    'users' => array(YII_DEBUG ? '*' : '@'),
                ),
            ),
            parent::accessRules()
        );
    }

    public function actionIndex()
    {
        $this->redirect('user/account');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if (\Yii::app()->getUser()->getIsGuest())
            $this->layout = 'guest/main';
        if ($error = \Yii::app()->getErrorHandler()->getError()) {
            if (\Yii::app()->getRequest()->getIsAjaxRequest()) {
                echo \CJSON::encode(array('error' => $error));
            } else {
                $this->render('error', $error);
            }
        }
    }
}