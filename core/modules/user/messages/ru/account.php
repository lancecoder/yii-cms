<?php
return array(
    //model attributes
    'ID' => 'ID',
    'Role' => 'Статус',
    'Phone' => 'Телефон',
    'Email' => 'Почта',
    'Password' => 'Пароль',
    'Activated' => 'Активирован',
    'Time Last Active' => 'Последняя активность',
    'Time Register' => 'Время регистрации',
    //breadcrumbs
    'Accounts' => 'Аккаунты',
    'Account #{id}' => 'Аккаунт #{id}',
    'Account' => 'Аккаунт',
    //menu
    'Manage' => 'Управление',
    'List' => 'Список',
    'Create' => 'Создание',
    'View #{id}' => 'Просмотр #{id}',
    'Update #{id}' => 'Редактирование #{id}',
    'Update' => 'Редактирование',
    'Delete #{id}' => 'Удаление #{id}',
    //ENUM ROLE
    'ADMIN' => 'Админ',
    'USER' => 'Участник',
    //Login form
    'Login error.' => 'Ошибка авторизации.',
    'Login' => 'Войти',
    'Logout' => 'Выйти',
);
