<?php
return array(
    //model attributes
    'Fk User Account' => 'ID',
    'Refer Code' => 'Код',
    'Lvl' => 'Вложенность',
    'Lft' => 'Левый индекс',
    'Rght' => 'Правый индекс',
    'Parent Id' => 'Родитель',
    'Parent' => 'Родитель',
    'Descendants' => 'Потомки',
    //breadcrumbs
    'Refers' => 'Связи',
    'Refer #{id}' => 'Связь #{id}',
    'Refer' => 'Структура',
    //menu
    'Manage' => 'Управление',
    'List' => 'Список',
    'Create' => 'Создание',
    'View #{id}' => 'Просмотр #{id}',
    'Update #{id}' => 'Редактирование #{id}',
    'Update' => 'Редактирование',
    'Delete #{id}' => 'Удаление #{id}',
);