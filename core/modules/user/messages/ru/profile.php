<?php
return array(
    //model attributes
    'First Name' => 'Имя',
    'Middle Name' => 'Отчество',
    'Last Name' => 'Фамилия',
    //breadcrumbs
    'Profiles' => 'Профили',
    'Profile #{id}' => 'Профиль #{id}',
    'Profile' => 'Профиль',
    //menu
    'Manage' => 'Управление',
    'List' => 'Список',
    'Create' => 'Создание',
    'View #{id}' => 'Просмотр #{id}',
    'Update #{id}' => 'Редактирование #{id}',
    'Update' => 'Редактирование',
    'Delete #{id}' => 'Удаление #{id}',
    //form error
    'All account have profile.' => 'Все аккаунты имеют профили.',
    'All account have refer' => 'Все аккаунты имеют рефералов',
    'Profile save' => 'Профиль изменен',
);