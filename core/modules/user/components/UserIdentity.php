<?php
namespace user\components;

use user\UserModule;

class UserIdentity extends \CUserIdentity
{
    private $_id;

    public function authenticate()
    {
        /* @var UserModule $userModule */
        $userModule = \Yii::app()->getModule('user');

        $criteria = new \CDbCriteria();
        if ($userModule->emailLogin)
            $criteria->compare('email', $this->username/*, false, 'OR'*/);

        if ($userModule->phoneLogin)
            $criteria->compare('phone', $this->username, false, 'OR');

        $users = \UserAccount::model();
        if (is_array($userModule->roleLogin))
            $users = $users->role($userModule->roleLogin);

        $users = \UserAccount::model()->active()->findAll($criteria);
        if ($users === array()) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
            foreach ($users as $user) {
                /* @var \UserAccount $user */
                if ($user->verifyPassword($this->password)) {
                    $this->_id = $user->id;
                    $this->username = $user->getUsername();
                    $this->errorCode = self::ERROR_NONE;
                }
            }
        }

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}