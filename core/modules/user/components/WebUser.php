<?php
namespace user\components;

use user\UserModule;

class WebUser extends \core\components\WebUser
{
    public $loginUrl = array('/user/account/login');
    public $logoutUrl = array('/user/account/logout');
    public function init()
    {
        parent::init();
        $this->guestName = UserModule::getRoleLabel(UserModule::ROLE_GUEST);
    }

    protected function loadModel()
    {
        return \UserAccount::model()->active()->findByPk($this->getId());
    }
}