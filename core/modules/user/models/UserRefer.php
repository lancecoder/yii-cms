<?php

/**
 * This is the model class for table "user_refer".
 *
 * The followings are the available columns in table 'user_refer':
 * @property integer $fk_user_account
 * @property integer $_lft
 * @property integer $_rght
 * @property integer $_lvl
 * @property string $refer_code
 *
 * The followings are the available model relations:
 * @property UserAccount $fkUserAccount
 */
class UserRefer extends CActiveRecord
{
    public function behaviors()
    {
        return array(
            'NestedSet' => array(
                'class' => 'ext.yiiext.NestedSet.NestedSetBehavior',
                'leftAttribute' => '_lft',
                'rightAttribute' => '_rght',
                'levelAttribute' => '_lvl',
            ),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserRefer the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_refer';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('_lft, _rght, _lvl', 'unsafe', 'except'=>'search'),
            array('fk_user_account, refer_code', 'required'),
            array('fk_user_account', 'numerical', 'integerOnly' => true),
            array('refer_code', 'length', 'max' => 7),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('fk_user_account, _lft, _rght, _lvl, refer_code', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'Account' => array(self::BELONGS_TO, 'UserAccount', 'fk_user_account'),
            'Profile' => array(self::HAS_ONE, 'UserProfile', array('fk_user_account' => 'fk_user_account')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'fk_user_account' => Yii::t('user.refer', 'Fk User Account'),
            '_lft' => Yii::t('user.refer', 'Lft'),
            '_rght' => Yii::t('user.refer', 'Rght'),
            '_lvl' => Yii::t('user.refer', 'Lvl'),
            'refer_code' => Yii::t('user.refer', 'Refer Code'),
            'parentId' => Yii::t('user.refer', 'Parent Id'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('fk_user_account', $this->fk_user_account);
//		$criteria->compare('_lft',$this->_lft);
//		$criteria->compare('_rght',$this->_rght);
//		$criteria->compare('_lvl',$this->_lvl);
        $criteria->compare('refer_code', $this->refer_code, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeValidate()
    {
        if ($isValid = parent::beforeValidate()) {
            if ($this->getIsNewRecord() && is_null($this->refer_code)) {
                $generated = false;
                do{
                    $code = substr(md5(time() + rand(0, 100)), -7);
                }while (self::model()->exists('refer_code = :code', array(':code' => $code)));
                $this->refer_code = $code;
            }
        }
        return $isValid;
    }

    /**
     * @return NestedSetBehavior
     */
    public function nestedSet()
    {
        return $this->asa('NestedSet');
    }

    /**
     * @return UserAccount
     */
    public function getAccount($refresh = false, $params = array())
    {
        return $this->getRelated('Account', $refresh, $params);
    }

    /**
     * @return UserProfile
     */
    public function getProfile($refresh = false, $params = array())
    {
        return $this->getRelated('Profile', $refresh, $params);
    }

}