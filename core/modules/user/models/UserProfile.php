<?php

/**
 * This is the model class for table "user_profile".
 *
 * The followings are the available columns in table 'user_profile':
 * @property integer $fk_user_account
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 *
 * The followings are the available model relations:
 * @property UserAccount $fkUserAccount
 */
class UserProfile extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserProfile the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_profile';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('fk_user_account, first_name, last_name', 'required'),
            array('fk_user_account', 'numerical', 'integerOnly' => true),
            array('first_name, middle_name', 'length', 'max' => 31),
            array('last_name', 'length', 'max' => 63),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('fk_user_account, first_name, middle_name, last_name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'Account' => array(self::BELONGS_TO, 'UserAccount', 'fk_user_account'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'fk_user_account' => \Yii::t('user.account', 'ID'),
            'first_name' => \Yii::t('user.profile', 'First Name'),
            'middle_name' => \Yii::t('user.profile', 'Middle Name'),
            'last_name' => \Yii::t('user.profile', 'Last Name'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('fk_user_account', $this->fk_user_account);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('middle_name', $this->middle_name, true);
        $criteria->compare('last_name', $this->last_name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}