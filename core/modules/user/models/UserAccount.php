<?php

/**
 * This is the model class for table "user_account".
 *
 * The followings are the available columns in table 'user_account':
 * @property integer $id
 * @property string $role
 * @property string $phone
 * @property string $email
 * @property string $password
 * @property integer $activated_email
 * @property integer $activated_phone
 * @property string $time_last_active
 * @property string $time_register
 *
 * The followings are the available model relations:
 * @property UserProfile $userProfile
 */
class UserAccount extends \core\components\ActiveRecord implements \core\components\IUserModel
{
    /**
     * Search range attributes
     */
    public $last_active_min;
    public $last_active_max;
    public $register_min;
    public $register_max;

    public function behaviors()
    {
        return array(
            'AuthManager' => 'user\\behaviors\\ar\\AuthManager',
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_account';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        /* @var \user\UserModule $userModule */
        $userModule = Yii::app()->getModule('user');
        $rules = array(
            array('id', 'unsafe',
                'except' => 'search'
            ),

            array('role', 'in', 'allowEmpty' => false,
                'range' => array_keys(self::getRoleLabels()),
                'on' => array('insert', 'update')
            ),
            array('role', 'in', 'allowEmpty' => true,
                'range' => array_keys(self::getRoleLabels()),
                'on' => 'search'
            ),

            array('phone', 'match', 'allowEmpty' => !$userModule->phoneLogin,
                'pattern' => '/^\+?\d{11}$/',
                'on' => array('insert', 'update')
            ),
            array('phone', 'exist', 'allowEmpty' => !$userModule->phoneLogin,
                'on' => array('insert', 'update')
            ),

            array('email', 'length', 'allowEmpty' => !$userModule->emailLogin,
                'min' => 6, 'max' => 127,
                'on' => array('insert', 'update')
            ),
            array('email', 'email', 'allowEmpty' => !$userModule->emailLogin,
                'on' => array('insert', 'update')
            ),
            array('email', 'exist', 'allowEmpty' => !$userModule->emailLogin,
                'on' => array('insert', 'update')
            ),
            //not crypted
            array('password', 'length', 'allowEmpty' => true,
                'min' => 6,
                'on' => 'update'
            ), // not changed
            array('password', 'length', 'allowEmpty' => false,
                'min' => 6,
                'on' => 'insert'
            ), // new password
            array('password', 'unsafe',
                'on' => 'search'
            ),

            array('activated_email, activated_phone', 'boolean', 'allowEmpty' => false,
                'on' => array('insert', 'update')
            ),
            array('activated_email, activated_phone', 'boolean', 'allowEmpty' => true,
                'on' => 'search'
            ),

            array('time_register', 'default', 'setOnEmpty' => false,
                'value' => new CDbExpression('NOW()'),
                'on' => 'insert'
            ),
            /*array('time_register', 'core\\validators\\Timestamp', 'allowEmpty' => false,
                'on' => array('update')
            ),*/
            array('time_register', 'unsafe',
                'on' => array('insert', 'update', 'search')
            ),

            array('time_last_active', 'core\\validators\\Timestamp', 'allowEmpty' => true,
                'on' => array('update')
            ),
            array('time_last_active', 'unsafe',
                'except' => 'update'
            ),

            array('last_active_min, last_active_max, register_min, register_max', 'unsafe', 'except' => 'search'),

            array(
                'id, phone, email, ',
                'safe',
                'on' => 'search'
            ),
        );
        return $rules;
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'Profile' => array(self::HAS_ONE, 'UserProfile', 'fk_user_account'),
            'Refer' => array(self::HAS_ONE, 'UserRefer', 'fk_user_account'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => \Yii::t('user.account', 'ID'),
            'role' => \Yii::t('user.account', 'Role'),
            'phone' => \Yii::t('user.account', 'Phone'),
            'email' => \Yii::t('user.account', 'Email'),
            'password' => \Yii::t('user.account', 'Password'),
            'activated_email' => \Yii::t('user.account', 'Activated Email'),
            'activated_phone' => \Yii::t('user.account', 'Activated Phone'),
            'time_last_active' => \Yii::t('user.account', 'Time Last Active'),
            'time_register' => \Yii::t('user.account', 'Time Register'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new \core\components\DbCriteria();

        $criteria->compare('id', $this->getPrimaryKey());

        if ($this->getAttribute('role') !== null)
            $criteria->scopes['role'] = array($this->getAttribute('role'));

        $criteria->compare('phone', $this->getAttribute('phone'), true);
        $criteria->compare('email', $this->getAttribute('email'), true);

        if ($this->getAttribute('activated_email') !== null)
            $criteria->scopes['activatedEmail'] = array($this->getAttribute('activated_email'));
        if ($this->getAttribute('activated_phone') !== null)
            $criteria->scopes['activatedPhone'] = array($this->getAttribute('activated_phone'));

        $criteria->addBetweenCondition('time_last_active', $this->last_active_min, $this->last_active_max);
        $criteria->addBetweenCondition('time_register', $this->register_min, $this->register_max);

        return $criteria;
    }

    //@todo: cache
    public static function getRoleLabels($defaultRoles = false)
    {
        /* @var CAuthManager $authManager */
        $authManager = Yii::app()->getAuthManager();
        $items = $authManager->getRoles();
        if ($defaultRoles)
            return $items;
        $roles = array();
        foreach ($items as $name => $item) {
            if (!in_array($name, $authManager->defaultRoles))
                $roles[$name] = Yii::t('auth.item.role', $name);
        }
        return $roles;
    }

    public function getUsername()
    {
        /* @var \user\UserModule $userModule */
        $userModule = Yii::app()->getModule('user');
        return $userModule->emailLogin ? $this->getAttribute('email') : $this->getAttribute('phone');
    }

    //@todo: cache on getRoleLabels
    public function getRole()
    {
        /* @var CAuthManager $authManager */
        $authManager = Yii::app()->getAuthManager();
        $item = $authManager->getAuthItem($this->getAttribute('role'));
        if ($item !== null && $item->getType() === CAuthItem::TYPE_ROLE)
            return Yii::t('auth.item.role', $item->getName());
        return $item;
    }

    public function verifyPassword($password)
    {
        return CPasswordHelper::verifyPassword($password, $this->getOldAttribute('password'));
    }

    protected function afterFind()
    {
        parent::afterFind();
        $this->setAttribute('password', null);
    }

    protected function afterSave()
    {
        parent::afterSave();
        $this->setAttribute('password', null);
        /* @var \user\UserModule $userModule */
        $userModule = Yii::app()->getModule('user');
        if ($userModule->emailLogin && !$this->getAttribute('activated_email') && $this->getAttribute('email')) {
            $this->sendActivateSMS();
        }
        if ($userModule->phoneLogin && !$this->getAttribute('activated_phone') && $this->getAttribute('phone')) {
            if (!$this->sendActivateEmail())
                throw new CException('send sms error');
        }
    }

    protected function beforeSave()
    {
        if (parent::beforeSave()) {
            if ($this->getAttribute('password') !== null)
                $this->setAttribute('password', CPasswordHelper::hashPassword($this->getAttribute('password')));
            else $this->setAttribute('password', $this->getOldAttribute('password'));
            return true;
        }
        return false;
    }

    //Scopes
    public function active()
    {
        /* @var \user\UserModule $userModule */
        $userModule = Yii::app()->getModule('user');
        $scopes = array();
        if ($userModule->emailLogin && $userModule->phoneLogin) {
            $scopes['activatedEmail'] = array(true);
            $scopes['activatedPhone'] = array(true, 'OR');
        } elseif ($userModule->emailLogin) {
            $scopes['activatedEmail'] = array(true);
        } elseif ($userModule->phoneLogin) {
            $scopes['activatedPhone'] = array(true);
        }
        $this->getDbCriteria()->mergeWith(
            array(
                'scopes' => $scopes,
            )
        );
        return $this;
    }

    public function activatedEmail($activated = true, $operator = 'AND')
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'activated_email = :activated',
                'params' => array(
                    ':activated' => $activated,
                ),
            ),
            $operator
        );
        return $this;
    }

    public function activatedPhone($activated = true, $operator = 'AND')
    {
        $this->getDbCriteria()->mergeWith(
            array(
                'condition' => 'activated_phone = :activated',
                'params' => array(
                    ':activated' => $activated,
                ),
            ),
            $operator
        );
        return $this;
    }

    public function role($role)
    {
        $criteria = new \core\components\DbCriteria();
        $criteria->addInCondition('role', $role);
        $this->getDbCriteria()->mergeWith($criteria);
        return $this;
    }

    public $activateEmailSubject = 'Activate Email';
    public $activateEmailTemplate = 'user.templates.email.activate';
    public $activateEmailFrom = null;

    public function sendActivateEmail()
    {
        //$this->activated_email
        return $this->sendEmail($this->activateEmailSubject, $this->activateEmailTemplate, $this, $this->activateEmailFrom);
    }

    public function sendEmail($subject, $template, $data, $from = null)
    {
        /* @var \user\UserModule $userModule */
        $userModule = Yii::app()->getModule('user');
        if ($from === null) {
            $from = \Yii::app()->params['adminEmail'];
        }
        $message = new \YiiMailMessage;
        $message->view = $template;
        $message->message->setSubject($subject);
        //get owner email
        $message->setBody($data, 'text/html');
        $message->addTo($this->getAttribute('email'));
        $message->message->setFrom($from);
        /* @var \YiiMail $mail */
        $mail = \Yii::app()->getComponent($userModule->emailComponent);
        return $mail->send($message);

    }
    public function sendActivateSMS()
    {

    }
    /**
     * @return UserProfile
     */
    public function getProfile($refresh = false, $params = array())
    {
        return $this->getRelated('Profile', $refresh, $params);
    }

    /**
     * @return UserRefer
     */
    public function getRefer($refresh = false, $params = array())
    {
        return $this->getRelated('Refer', $refresh, $params);
    }
}