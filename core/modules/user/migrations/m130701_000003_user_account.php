<?php

class m130701_000003_user_account extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'user_account',
            array(
                'id' => 'INT(11) UNSIGNED NOT NULL',
                'role' => 'ENUM("ADMIN", "MODERATOR", "USER") NOT NULL',
                'username' => 'VARCHAR(15)',
                'phone' => 'VARCHAR(11)',
                'email' => 'VARCHAR(127)',
                'password' => 'VARCHAR(255) NOT NULL',
                'activated_email' => 'TINYINT(1) NOT NULL',
                'activated_phone' => 'TINYINT(1) NOT NULL',
                'time_last_active' => 'TIMESTAMP',
                'time_register' => 'TIMESTAMP',
            ),
            'ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci'
        );
        $this->addPrimaryKey('pk_user_account', 'user_account', 'id');
        $this->createIndex('uniq1_user_account', 'user_account', 'phone', true);
        $this->createIndex('uniq2_user_account', 'user_account', 'email, phone', true);
        $this->alterColumn('user_account', 'id', 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT');
        $admin = array(
            'role' => 'ADMIN',
            'phone' => '89375658171',
            'email' => 'lancecoder@mail.ru',
            'password' => CPasswordHelper::hashPassword('admin'),
            'activated_email' => 1,
            'activated_phone' => 1,
            'time_register' => new CDbExpression('NOW()'),
        );
        $admin1 = array_merge(
            $admin,
            array(
                'role' => 'USER',
                'phone' => '89223501350',
                'email' => '89223501350@gmail.ru',
            )
        );
        $admin2 = array_merge(
            $admin,
            array(
                'role' => 'USER',
                'phone' => '89223378873',
                'email' => '89223378873@gmail.ru',
                'activated' => 0
            )
        );
        $this->insert('user_account', $admin);
        //$this->insert('user_account', $admin1);
        $this->insert('user_account', $admin2);
    }

    public function safeDown()
    {
        $this->dropTable('user_account');
    }
}