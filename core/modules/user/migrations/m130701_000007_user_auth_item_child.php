<?php

class m130701_000007_user_auth_item_child extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'user_auth_item_child',
            array(
                'parent' => 'varchar(64) not null',
                'child' => 'varchar(64) not null',
            ),
            'ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci'
        );
        $this->addPrimaryKey('pk_user_auth_item_child', 'user_auth_item_child', 'parent,child');
        $this->addForeignKey('fk1_user_auth_item_child', 'user_auth_item_child', 'parent', 'user_auth_item', 'name');
        $this->addForeignKey('fk2_user_auth_item_child', 'user_auth_item_child', 'child', 'user_auth_item', 'name');
        $this->insert(
            'user_auth_item_child',
            array(
                'parent' => 'ADMIN',
                'child' => 'MODERATOR',
            )
        );
        $this->insert(
            'user_auth_item_child',
            array(
                'parent' => 'MODERATOR',
                'child' => 'USER',
            )
        );
        $this->insert(
            'user_auth_item_child',
            array(
                'parent' => 'USER',
                'child' => 'GUEST',
            )
        );
    }

    public function safeDown()
    {
        $this->dropTable('user_auth_item_child');
    }
}