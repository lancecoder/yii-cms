<?php

class m130701_000005_user_task extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'user_task',
            array(
                'id' => 'INT(11) UNSIGNED NOT NULL',
                'fk_user_account' => 'INT(11) UNSIGNED NOT NULL',
                'type' => 'ENUM("EMAIL_ACTIVATE", "PHONE_ACTIVATE", "PHONE_PASSWORD", "EMAIL_PASSWORD") NOT NULL',
                'hash' => 'VARCHAR(32)',
                'time_created' => 'TIMESTAMP',
            ),
            'ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci'
        );
        $this->addPrimaryKey('pk_user_task', 'user_task', 'id');
        $this->addForeignKey('fk1_user_task', 'user_task', 'fk_user_account', 'user_account', 'id');
        $this->createIndex('uniq1', 'user_task', 'fk_user_account, type', true);
        $this->alterColumn('user_task', 'id', 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT');
    }

    public function safeDown()
    {
        $this->dropTable('user_task');
    }
}