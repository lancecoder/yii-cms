<?php

class m130701_000006_user_auth_item extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'user_auth_item',
            array(
                'name' => 'VARCHAR(64) NOT NULL',
                'type' => 'INT(11) NOT NULL',
                'description' => 'TEXT',
                'bizrule' => 'TEXT',
                'data' => 'TEXT',
            ),
            'ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci'
        );
        $this->addPrimaryKey('pk_user_auth_item', 'user_auth_item', 'name');
        $this->insert(
            'user_auth_item',
            array(
                'name' => 'ADMIN',
                'type' => 2,
                'description' => 'Administrator',
            )
        );
        $this->insert(
            'user_auth_item',
            array(
                'name' => 'MODERATOR',
                'type' => 2,
                'description' => 'Moderator',
            )
        );
        $this->insert(
            'user_auth_item',
            array(
                'name' => 'USER',
                'type' => 2,
                'description' => 'Member',
            )
        );
        $this->insert(
            'user_auth_item',
            array(
                'name' => 'GUEST',
                'type' => 2,
                'description' => 'Member',
            )
        );
    }

    public function safeDown()
    {
        $this->dropTable('user_auth_item');
    }
}