<?php

class m130701_000009_user_refer extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'user_refer',
            array(
                'fk_user_account' => 'INT(11) UNSIGNED NOT NULL',
                '_lft' => 'INT(11) NOT NULL',
                '_rght' => 'INT(11) NOT NULL',
                '_lvl' => 'INT(11) NOT NULL',
                '_order' => 'TINYINT(1) NOT NULL',
                'refer_code' => 'VARCHAR(7) NOT NULL',
            ),
            'ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci'
        );
        $this->addPrimaryKey('pk_user_refer', 'user_refer', 'fk_user_account');
        $this->createIndex('uniq1_user_refer', 'user_refer', 'fk_user_account', true);
        $this->addForeignKey('fk1_user_refer', 'user_refer', 'fk_user_account', 'user_account', 'id');
        $this->createIndex('uniq2_user_refer', 'user_refer', 'refer_code', true);
        /*$this->insert('user_refer', array(
            'fk_user_account' => 1,
            '_lft'=>1,
            '_rght'=>6,
            '_lvl'=>1,
            'refer_code'=>substr(md5(time()),-7),
        ));
        $this->insert('user_refer', array(
            'fk_user_account' => 2,
            '_lft'=>2,
            '_rght'=>3,
            '_lvl'=>2,
            'refer_code'=>substr(md5(time()+1),-7),
        ));
        $this->insert('user_refer', array(
            'fk_user_account' => 3,
            '_lft'=>4,
            '_rght'=>5,
            '_lvl'=>2,
            'refer_code'=>substr(md5(time()+2),-7),
        ));*/
    }

    public function safeDown()
    {
        $this->dropTable('user_refer');
    }
}