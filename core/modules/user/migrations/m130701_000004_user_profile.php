<?php

class m130701_000004_user_profile extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'user_profile',
            array(
                'fk_user_account' => 'INT(11) UNSIGNED NOT NULL',
                'first_name' => 'VARCHAR(32) NOT NULL',
                'middle_name' => 'VARCHAR(32)',
                'last_name' => 'VARCHAR(64) NOT NULL',
            ),
            'ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci'
        );
        $this->addPrimaryKey('pk_user_profile', 'user_profile', 'fk_user_account');
        $this->createIndex('uniq1_user_profile', 'user_profile', 'fk_user_account', true);
        $this->addForeignKey('fk1_user_profile', 'user_profile', 'fk_user_account', 'user_account', 'id');
        $this->insert(
            'user_profile',
            array(
                'fk_user_account' => '1',
                'first_name' => 'Lance',
                'last_name' => 'Coder',
            )
        );
    }

    public function safeDown()
    {
        $this->dropTable('user_profile');
    }
}