<?php

class m130701_000008_user_auth_assign extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'user_auth_assign',
            array(
                'itemname' => 'VARCHAR(64) NOT NULL',
                'userid' => 'VARCHAR(64) NOT NULL',
                'bizrule' => 'TEXT',
                'data' => 'TEXT',
            ),
            'ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci'
        );
        $this->addPrimaryKey('pk_user_auth_assign', 'user_auth_assign', 'itemname,userid');
        $this->addForeignKey(
            'fk1_user_auth_assign',
            'user_auth_assign',
            'itemname',
            'user_auth_item',
            'name',
            'cascade',
            'cascade'
        );
        $this->insert('user_auth_assign', array(
            'itemname' => 'ADMIN',
            'userid' => 1,
        ));
        $this->insert('user_auth_assign', array(
            'itemname' => 'ADMIN',
            'userid' => 2,
        ));
    }

    public function safeDown()
    {
        $this->dropTable('user_auth_assign');
    }
}