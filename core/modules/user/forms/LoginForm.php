<?php

class LoginForm extends \core\components\FormModel
{
    public $username;
    public $password;
    private $_identity = null;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array('username', 'validateUsername',),
            array('password', 'validatePassword'),
        );
    }

    public function validateUsername($attribute, $params)
    {
        $valid = null;
        /* @var \user\UserModule $userModule */
        $userModule = Yii::app()->getModule('user');
        if ($userModule->emailLogin && strstr($this->username, '@')) {
            $validator = new CEmailValidator();
            $validator->allowEmpty = false;
            $valid = $validator->validateValue($this->username);
        } elseif ($userModule->phoneLogin) {
            $valid = preg_match('/^\d{11}$/', $this->username);
        }
        if (!$valid)
            $this->loginFailed();
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors() && $this->_identity === null) {
            $identity = new \user\components\UserIdentity($this->username, $this->password);
            if (!$identity->authenticate()) {
                $this->loginFailed();
            } else {
                $this->_identity = $identity;
            }
        }
    }

    private function loginFailed()
    {
        $this->addError('username', 'Login failed');
        $this->addError('password', 'Login failed');
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'username' => Yii::t('user.loginAttribute', 'Login'),
            'password' => Yii::t('user.loginAttribute', 'Password'),
        );
    }


    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {
        if ($this->validate() && $this->_identity !== null) {
            $login = Yii::app()->getUser()->login($this->_identity);
            if (!$login)
                $this->loginFailed();
            return $login;
        }
        return false;
    }

}