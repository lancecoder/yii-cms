<?php
namespace user;

use core\components\WebModule;

class UserModule extends WebModule
{
    const ROLE_GUEST = 'GUEST';
    const ROLE_ADMIN = 'ADMIN';
    const ROLE_USER = 'USER';
    const ROLE_MODERATOR = 'MODERATOR';

    public $menuIcon = 'user white';
    public $menuType = 'inverse';

    public $emailLogin = true;
    public $phoneLogin = true;

    //allowed roles to login
    public $roleLogin;

    public $emailActivation = true;
    public $phoneActivation = true;

    public $emailComponent = 'mail';
    public $phoneComponent = 'sms';

    public function init()
    {
        parent::init();
        //\Yii::app()->getDb()->getCommandBuilder()->getSchema()->getTable('user_account')->getColumn('role')
        if (!$this->emailLogin && !$this->phoneLogin)
            throw new \CException('detect fields for login');
        $this->setImport(
            array(
                'user.models.*',
                'user.forms.*',
            )
        );
    }

    /*public function getMenuItems()
    {
        return array(
            array(
                'label' => \Yii::t('user.account', 'Accounts'),
                'icon' => 'user',
                'url' => array('/user/account/')
            ),
            array(
                'label' => \Yii::t('user.profile', 'Profiles'),
                'icon' => 'briefcase',
                'url' => array('/user/profile/')
            ),

            array(
                'label' => \Yii::t('user.refer', 'Refers'),
                'icon' => 'random',
                'url' => array('/user/refer/')
            ),
        );
    }*/

    /**
     * @return array all roles
     */
    public static function getRoleLabels()
    {
        return array(
            self::ROLE_GUEST => \Yii::t('user.role', 'GUEST'),
            self::ROLE_ADMIN => \Yii::t('user.role', 'ADMIN'),
            self::ROLE_MODERATOR => \Yii::t('user.role', 'MODERATOR'),
            self::ROLE_USER => \Yii::t('user.role', 'USER'),
        );
    }

    /**
     * @param string $role
     * @return string
     */
    public static function getRoleLabel($role)
    {
        $roles = self::getRoleLabels(true);
        if (isset($roles[$role])) {
            return $roles[$role];
        }
        return null;
    }

    /**
     * @param string $email
     * @param string $template
     * @param string $subject
     * @param array $data
     * @param string|null $from
     * @return boolean
     */
    /*public static function sendEmail($email, $template, $subject, array $data, $from = null)
    {
        if ($from === null) {
            $from = \Yii::app()->params['adminEmail'];
        }
        $message = new \YiiMailMessage;
        $message->view = $template;
        $message->message->setSubject($subject);
        //get owner email
        $message->setBody($data, 'text/html');
        $message->addTo($email);
        $message->message->setFrom($from);
        /* @var \YiiMail $mail */
    /*
            $mail = \Yii::app()->getComponent($this->emailComponent);
            return (bool)$mail->send($message);
        }*/
}