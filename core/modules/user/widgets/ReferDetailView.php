<?php
namespace user\widgets;

class ReferDetailView extends \CWidget
{
    public $depth=2;
    public $data;
    public $attributes;
    public $condition;

    public function init()
    {
        $this->condition = array_merge($this->condition, array('index' => '_order'));
    }
    public function run()
    {
        $this->renderChildren($this->data);
    }

    public function renderChildren(\UserRefer $parent)
    {
        echo '<table><tr>';
        $models = $parent->nestedSet()->children()->findAll($this->condition);
        foreach (array(0,1) as $key) {
            echo '<td width="50%" style="vertical-align: top;">';
            if (!array_key_exists($key, $models))
                echo '&nbsp;';
            else {
                $this->widget('bootstrap.widgets.TbDetailView',
                    array(
                        'data' => $models[$key],
                        'attributes' => $this->attributes,
                    )
                );
                if ($models[$key]->_lvl - $this->data->_lvl < $this->depth)
                    $this->renderChildren($models[$key]);
            }
            echo '</td>';
        }
        echo '</tr></table>';
    }
}