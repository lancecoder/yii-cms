<?php
namespace user\behaviors\ar;

use core\components\ActiveRecord;

class AuthManager extends \CActiveRecordBehavior
{
    public $roleAttribute = 'role';

    protected function afterSave()
    {
        $owner = $this->getOwner();
        if ($owner instanceof ActiveRecord) {
            $authManager = \Yii::app()->getAuthManager();
            $pk = $owner->getPrimaryKey();
            $role = $owner->getAttribute($this->roleAttribute);
            if ($owner->getIsNewRecord() && !$authManager->isAssigned($role, $pk)) {
                $authManager->assign($role, $pk);
            } else {
                if ($owner->isAttributeChanged($this->roleAttribute) && $authManager->isAssigned($owner->getOldAttribute($this->roleAttribute), $pk)) {
                    $authManager->revoke($owner->getOldAttribute($this->roleAttribute), $pk);
                }
            }
        }
    }

    protected function afterDelete()
    {
        $owner = $this->getOwner();
        if ($owner instanceof ActiveRecord) {
            $authManager = \Yii::app()->getAuthManager();
            $pk = $owner->getPrimaryKey();
            foreach ($authManager->getAuthAssignments($pk) as $assignment) {
                /* @var \CAuthAssignment $assignment*/
                $authManager->revoke($assignment->getItemName(), $pk);
            }
        }
    }
}