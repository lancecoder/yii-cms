<?php
namespace core\components;

/**
 * Class WebUser
 * @package core\components
 * @property ActiveRecord $model
 */
abstract class WebUser extends \CWebUser
{
    private $_model = null;
    public $logoutUrl;
    /**
     * @return ActiveRecord
     */
    public function getModel()
    {
        if (!$this->getIsGuest() && $this->_model === null) {
            $this->_model = $this->loadModel();
            if (!$this->_model instanceof IUserModel)
                throw new \CException('core', 'Invalid User model');
        }
        return $this->_model;
    }

    public function getRole()
    {
        $model = $this->getModel();
        if (!$model instanceof IUserModel)
            throw new \CException(\Yii::t('core', 'Invalid User model'));
        return $model->getRole();
    }

    public function getMenuItems()
    {
        return array(
            array(
                'label' => $this->getName(),
                'icon' => 'off',
                'type' => 'danger',
                'url' => $this->logoutUrl,
                'visible' => !$this->getIsGuest()
            ),
            array(
                'label' => \Yii::t('core', 'Login'),
                'icon' => 'on',
                'type' => 'primary',
                'url' => $this->loginUrl,
                'visible' => $this->getIsGuest()
            ),
        );
    }
    /**
     * @return ActiveRecord
     */
    abstract protected function loadModel();
}