<?php
namespace core\components;

abstract class Controller extends \CController
{
    private $_menu = array();
    private $_pageTitle = null;
    private $_breadcrumbs = array();
    public $menuIcon;

    public function setMenu(array $menu)
    {
        $this->_menu = $menu;
    }

    public function getName()
    {
        return ucfirst($this->getId());
    }

    /* @return array */
    public function getMenuItems()
    {
        return $this->_menu;
    }

    public function setBreadcrumbs(array $breadcrumbs)
    {
        $this->_breadcrumbs = $breadcrumbs;
    }

    /* @return array */
    public function getBreadcrumbs()
    {
        if ($this->_breadcrumbs === array() || !is_array($this->_breadcrumbs)) {
            $um = \Yii::app()->getUrlManager();
            $m = $this->getModule();
            $c = $this->getId();
            if ($m !== null) {
                $m = $m->getId();
                $this->_breadcrumbs = array(
                    $m => $um->createUrl($m),
                    $c => $um->createUrl($m . '/' . $c),
                );
            } else {
                $this->_breadcrumbs = array(
                    $c => $um->createUrl($c)
                );
            }
            $this->_breadcrumbs[] = $this->getAction()->getId();
        }

        return $this->_breadcrumbs;
    }

    public function setPageTitle($title)
    {
        if (is_string($title)) {
            $this->_pageTitle = $title;
        }
    }

    /* @return string */
    public function getPageTitle($app = false, $separator = '::')
    {
        $name = array();
        if ($app) {
            $name[] = \Yii::app()->name;
        }
        $name[] = $this->_pageTitle !== null ? $this->_pageTitle : ucfirst(basename($this->getId()));

        return implode($separator, $name);
    }

    protected function modelNotFound($category = 'core', $message = 'The requested page does not exist.', $params = array())
    {
        throw new \CHttpException(404, \Yii::t($category, $message, $params));
    }
}