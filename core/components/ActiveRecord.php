<?php
namespace core\components;

abstract class ActiveRecord extends \CActiveRecord
{
    public static function model($className = null)
    {
        if ($className === null)
            $className = get_called_class();
        return parent::model($className);
    }

    /**
     * @var array old attribute values indexed by attribute names.
     */
    private $_oldAttributes;

    /**
     * Returns the attribute values that have been modified since they are loaded or saved most recently.
     * @param string[]|null $names the names of the attributes whose values may be returned if they are
     * changed recently. If null, [[attributes()]] will be used.
     * @return array the changed attribute values (name-value pairs)
     */
    public function getDirtyAttributes($names = null)
    {
        if ($names === null) {
            $names = array_keys($this->getMetaData()->columns);
        }
        $names = array_flip($names);
        $attributes = array();
        if ($this->_oldAttributes === null) {
            foreach ($this->getAttributes() as $name => $value) {
                if (isset($names[$name])) {
                    $attributes[$name] = $value;
                }
            }
        } else {
            foreach ($this->getAttributes() as $name => $value) {
                if (isset($names[$name]) && (!array_key_exists(
                            $name,
                            $this->_oldAttributes
                        ) || $value !== $this->_oldAttributes[$name])
                ) {
                    $attributes[$name] = $value;
                }
            }
        }
        return $attributes;
    }

    /**
     * Returns a value indicating whether the named attribute has been changed.
     * @param string $name the name of the attribute
     * @return boolean whether the attribute has been changed
     */
    public function isAttributeChanged($name)
    {
        if (!isset($this->getMetaData()->columns[$name])) {
            throw new \CException('column not found');
        }
        if (isset($this->_oldAttributes[$name])) {
            return $this->getMetaData()->columns[$name] !== $this->_oldAttributes[$name];
        }
        return false;
    }

    /**
     * Returns the old value of the named attribute.
     * If this record is the result of a query and the attribute is not loaded,
     * null will be returned.
     * @param string $name the attribute name
     * @return mixed the old attribute value. Null if the attribute is not loaded before
     * or does not exist.
     * @see hasAttribute
     */
    public function getOldAttribute($name)
    {
        return isset($this->_oldAttributes[$name]) ? $this->_oldAttributes[$name] : null;
    }

    /**
     * Returns the old attribute values.
     * @return array the old attribute values (name-value pairs)
     */
    public function getOldAttributes()
    {
        return $this->_oldAttributes === null ? array() : $this->_oldAttributes;
    }

    /**
     * This method is invoked after saving a record successfully.
     * The default implementation raises the {@link onAfterSave} event.
     * You may override this method to do postprocessing after record saving.
     * Make sure you call the parent implementation so that the event is raised properly.
     */
    protected function afterSave()
    {
        $this->_oldAttributes = $this->getAttributes();
        parent::afterSave();
    }

    /**
     * This method is invoked after a model instance is created by new operator.
     * The default implementation raises the {@link onAfterConstruct} event.
     * You may override this method to do postprocessing after model creation.
     * Make sure you call the parent implementation so that the event is raised properly.
     */
    protected function afterConstruct()
    {
        $this->_oldAttributes = $this->getAttributes();
        parent::afterConstruct();
    }

    /**
     * This method is invoked after each record is instantiated by a find method.
     * The default implementation raises the {@link onAfterFind} event.
     * You may override this method to do postprocessing after each newly found record is instantiated.
     * Make sure you call the parent implementation so that the event is raised properly.
     */
    protected function afterFind()
    {
        $this->_oldAttributes = $this->getAttributes();
        parent::afterFind();
    }

    /**
     * This method is invoked after deleting a record.
     * The default implementation raises the {@link onAfterDelete} event.
     * You may override this method to do postprocessing after the record is deleted.
     * Make sure you call the parent implementation so that the event is raised properly.
     */
    protected function afterDelete()
    {
        $this->_oldAttributes = null;
        parent::afterDelete();
    }

    /**
     * Addon for dynamic validators. @todo: testing or deleting
     * Create validators for current scenario
    /** @var \CList */
    private $_ruleValidators;
    private $_loadedRules = array();

    public function getValidatorList($refresh = false)
    {
        if ($refresh) {
            $this->_ruleValidators = null;
        }
        if ($this->_ruleValidators === null) {
            $this->createValidators();
        }
        return $this->_ruleValidators;
    }

    public function getValidators($attribute = null)
    {
        if ($this->_ruleValidators === null) {
            $this->createValidators();
        }
        $validators = array();
        $scenario = $this->getScenario();
        $rules = $this->applyScenario($this->rules());
        foreach ($rules as $ruleItem => $rule) {
            if (!in_array($ruleItem, $this->_loadedRules)) {
                if (isset($rule[0], $rule[1])) // attributes, validator name
                {
                    $listItem = $this->_ruleValidators->add(
                        \CValidator::createValidator($rule[1], $this, $rule[0], array_slice($rule, 2))
                    );
                } else {
                    throw new \CException(\Yii::t(
                        'yii',
                        '{class} has an invalid validation rule. The rule must specify attributes to be validated and the validator name.',
                        array('{class}' => get_class($this))
                    ));
                }
                $this->_loadedRules[$listItem] = $ruleItem;
            } else {
                $listItem = array_search($ruleItem, $this->_loadedRules);
            }
            $validator = $this->_ruleValidators->itemAt($listItem);
            if ($validator->applyTo($scenario)) {
                if ($attribute === null || in_array($attribute, $validator->attributes, true)) {
                    $validators[] = $validator;
                }
            }
        }
        return $validators;
    }

    public function createValidators()
    {
        if ($this->_ruleValidators === null) {
            $this->_ruleValidators = new \CList();
        }
        return $this->getValidators();
    }

    protected function applyScenario(array $items)
    {
        foreach ($items as $i => $item) {
            if (isset($item['except']) && $this->applyTo($item, 'except')) {
                unset($items[$i]);
            } else {
                if (!empty($item['on']) && !$this->applyTo($item, 'on')) {
                    unset($items[$i]);
                }
            }
        }
        return $items;
    }

    private function applyTo($item, $key = 'on')
    {
        $scenario = $this->getScenario();
        $on = $item[$key];
        if (is_string($on)) {
            $on = preg_split('/\s*,\s*/', $on, -1, PREG_SPLIT_NO_EMPTY);
        }

        if ($on !== array() && !in_array($scenario, $on)) {
            return false;
        }
        return true;
    }
}