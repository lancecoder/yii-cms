<?php
namespace core\components;

\Yii::import('bootstrap.widgets.TbActiveForm');
class ActiveForm extends \TbActiveForm{

    public function inputRow($type, \CModel $model, $attribute, $data = null, $htmlOptions = array())
    {
        if ($type !== \TbInput::TYPE_FILE && !$model->isAttributeSafe($attribute) && !isset($htmlOptions['disabled']))
            $htmlOptions['disabled'] = 'disabled';
        return parent::inputRow($type, $model, $attribute, $data, $htmlOptions);
    }
}