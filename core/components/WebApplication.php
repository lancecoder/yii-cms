<?php
namespace core\components;

abstract class WebApplication extends \CWebApplication
{
    /**
     * Black list modules @see self::initModuleAliases(), @see self::getMenuItems()
     * @var array
     */
    public $modulesBlackList = array('gii');

    protected function init()
    {
        parent::init();
        $this->setModuleAliasRecursive($this);
    }

    /**
     * Items for main menu widget can be use in layout
     * @return array
     */
    public function getMenuItems()
    {
        //@todo: cache
        $items = array();
        foreach ($this->getModules() as $id => $config) {
            if (in_array($id, $this->modulesBlackList)) {
                //ignore gii WebUser and other left modules
                continue;
            }
            $module = $this->getModule($id);
            if ($module instanceof WebModule) {
                $moduleItems = $module->getMenuItems();
                if ($moduleItems !== array()) {
                    $items[] = array(
                        'label' => \Yii::t($module->getId(), $module->getName()),
                        'url' => $this->getUrlManager()->createUrl($module->getId()),
                        'icon' => $module->menuIcon,
                        'type' => $module->menuType,
                        'items' => $moduleItems
                    );
                }
            }
        }
        $userItems = \Yii::app()->getUser()->getMenuItems();
        if ($userItems !== array())
            $items = \CMap::mergeArray($items, $userItems);
        return $items;
    }

    /**
     * Init aliases from module namespaces
     * @param \CModule $parent
     */
    private function setModuleAliasRecursive(\CModule $parent)
    {
        //@todo: cache
        foreach ($parent->getModules() as $id => $config) {
            if (in_array($id, $this->modulesBlackList)) {
                continue;
            }
            $module = $parent->getModule($id);
            \Yii::setPathOfAlias($id, $module->getBasePath());
            $function = __FUNCTION__;
            $this->$function($module);
        }
    }

    /* @return WebUser */
    public function getUser()
    {
        return parent::getUser();
    }
}