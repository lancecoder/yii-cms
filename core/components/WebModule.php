<?php
namespace core\components;

abstract class WebModule extends \CWebModule
{
    /**
     * @return array items for App Menu
     */
    public function getMenuItems()
    {
        $items = array();
        foreach ($this->controllerMap as $id => $controller) {
            /* @var $controller Controller */
            $controller = \Yii::createComponent($controller,$id,$this);
            if ($controller instanceof Controller) {
                $controllerItems = $controller->getMenuItems();
                if ($controllerItems !== array())
                    $items[] = array(
                        'label' => \Yii::t($controller->getUniqueId(), $controller->getName()),
                        'icon' => $controller->menuIcon,
                        'url' => \Yii::app()->getUrlManager()->createUrl($controller->getUniqueId()),
                    );
            }
        }
        return $items;
    }
    public $menuIcon;
}