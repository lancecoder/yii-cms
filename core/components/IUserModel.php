<?php
namespace core\components;

interface IUserModel
{
    public function getRole();

    public function verifyPassword($password);

    public function getUsername();

    //scope
    public function active();
}