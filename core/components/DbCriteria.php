<?php
namespace core\components;

class DbCriteria extends \CDbCriteria
{
    public function addInCondition($column, $values, $operator='AND')
    {
        if ($values === array() || $values === null)
            return $this;
        if (is_array($values))
            return parent::addInCondition($column, $values, $operator);
        else
            return parent::compare($column, $values, $operator);
    }

    public function addBetweenCondition($column, $valueStart, $valueEnd, $operator='AND')
    {
        if ($valueStart === null && $valueEnd === null)
            return $this;

        if ($valueStart === null)
            $valueStart = mktime(0, 0, 1, 1, 1, 1970);
        else
            $valueStart = strtotime($valueStart);

        if ($valueEnd === null)
            $valueEnd = time();
        else
            $valueEnd = strtotime($valueEnd);

        return parent::addBetweenCondition($column, $valueStart, $valueEnd);
    }
}
