<?php

class m130701_000001_message_source extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'message_source',
            array(
                'id' => 'INT(11) UNSIGNED NOT NULL',
                'category' => 'VARCHAR(31) NOT NULL',
                'message' => 'TEXT NOT NULL',
            ),
            'ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci'
        );
        $this->addPrimaryKey('pk_message_source', 'message_source', 'id');
        $this->alterColumn('message_source', 'id', 'INT(11) UNSIGNED NOT NULL AUTO_INCREMENT');
    }

    public function safeDown()
    {
        $this->dropTable('message_source');
    }
}