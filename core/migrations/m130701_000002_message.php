<?php

class m130701_000002_message extends CDbMigration
{
    public function safeUp()
    {
        $this->createTable(
            'message',
            array(
                'id' => 'INT(11) UNSIGNED NOT NULL',
                'language' => 'VARCHAR(15) NOT NULL',
                'translation' => 'TEXT NOT NULL',
            ),
            'ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci'
        );
        $this->addPrimaryKey('pk_message', 'message', 'id, language');
        $this->addForeignKey('fk1_message', 'message', 'id', 'message_source', 'id', 'CASCADE', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropTable('message');
    }
}