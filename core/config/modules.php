<?php
return array(
    'gii' => array(
        'class' => 'system.gii.GiiModule',
        'password' => 'lance',
        // 'ipFilters'=>array(…список IP…),
        // 'newFileMode'=>0666,
        // 'newDirMode'=>0777,
    ),
    'user' => array(
        'viewPath' => LC_APP_PATH . LC_DS . 'modules' . LC_DS . 'user' . LC_DS . 'views',
        'controllerPath' => LC_APP_PATH . LC_DS . 'modules' . LC_DS . 'user' . LC_DS . 'controllers',
        'controllerNamespace' => LC_APP_ID . '\\modules\\user\\controllers',
        'class' => 'user\\UserModule',
    ),
);