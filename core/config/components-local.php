<?php
return array(
    'db' => array(
        'connectionString' => 'mysql:host=localhost;dbname=life',
        'enableProfiling' => true,
        'emulatePrepare' => true,
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
    ),
    'log' => array(
        'class' => 'system.logging.CLogRouter',
        'routes' => array(
            array(
                'class' => 'system.logging.CWebLogRoute',
                'showInFireBug' => false,
                'enabled' => YII_DEBUG,
                'levels'=>'profile',
                'categories'=>'system.db.*',
            ),
            /*array(
                'class'=>'system.logging.CProfileLogRoute',
                'enabled'=>YII_DEBUG,
                'levels'=>'trace, info, error, warning',
                //'categories'=>'system.*',
            ),*/
        ),
    ),
);