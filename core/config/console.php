<?php
// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array_merge(
    array(
        'basePath' => dirname(
            __FILE__
        ) . DIRECTORY_SEPARATOR . '..' /*. DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'user'*/,
        'name' => 'My Console Application',
        'components' => array(),
        'modules' => array(
            'user' => array(
                'class' => 'user\\UserModule',
                'import' => array(
                    'user.models.*',
                    'user.forms.*',
                ),
            ),
        )
    ),
    require 'console-local.php'
);