<?php
require(LC_CORE_PATH . LC_DS . 'modules' . LC_DS . 'user' . LC_DS . 'UserModule.php');
return array_merge(
    array(
        'user' => array(
            'class' => 'user\\components\\WebUser',
        ),
        'authManager' => array(
            /*'class' => 'user\\components\\AuthManager',*/
            'class' => 'CDbAuthManager',
            'defaultRoles' => array(\user\UserModule::ROLE_GUEST),
            'assignmentTable' => 'user_auth_assign',
            'itemTable' => 'user_auth_item',
            'itemChildTable' => 'user_auth_item_child',
        ),
        'request' => array(
            'enableCookieValidation' => true,
            'enableCsrfValidation' => true,
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'showScriptName' => false,
        ),
        'errorHandler' => array(
            'errorAction' => 'site/error',
        ),
        'messages' => array(
            'class' => 'CDbMessageSource',
            'sourceMessageTable' => 'message_source',
            'translatedMessageTable' => 'message',
        ),
        'cache' => array(
            'class' => 'system.caching.CDummyCache',
            //'connectionID' => 'db'
        ),

        'mail' => array(
            'class' => 'ext.yii-mail.YiiMail',
            'transportType' => 'smtp',
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => 'lancecoder.yandex@gmail.com',
                'password' => 'PrivetLancecoder',
                'port' => '465',
                'encryption' => 'tls',
            ),
            'viewPath' => 'core.mails',
            'logging' => true,
            'dryRun' => false
        ),
        'sms' => array
        (
            'class' => 'ext.LittleSMS.LittleSMS',
            'user' => 'anton', // Основной или дополнительный аккаунт
            'apikey' => 'qazzqwertasddsfdsfewsd', // API-ключ аккаунта
            'testMode' => false // Режим тестирования по умолчанию выключен, будьте внимательны
        ),
    )
    ,
    require 'components-local.php'
);