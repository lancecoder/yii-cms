<?php
return array(
    'name' => ucfirst(LC_APP_ID),
    'id' => LC_APP_ID,
    'homeUrl' => '/' . LC_APP_ID . '/',
    'controllerNamespace' => LC_APP_ID . '\\controllers',
    'basePath' => LC_APP_PATH,
    'extensionPath' => LC_CORE_PATH . LC_DS . 'extensions',
    'modulePath' => LC_CORE_PATH . LC_DS . 'modules',
    'sourceLanguage' => 'en_us',
    'import' => array(
        'ext.yii-mail.*',
        'ext.LittleSMS.*',
    ),
    'modules' => require 'modules.php',
    'components' => require 'components.php',
    'params' => require 'params.php',
);