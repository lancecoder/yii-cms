<?php
/****** PATHS **********/
define('LC_DS', DIRECTORY_SEPARATOR);
define('LC_WEB_PATH', dirname(__FILE__));
define('LC_ROOT_PATH', LC_WEB_PATH . LC_DS . '..');
define('LC_APP_PATH', LC_ROOT_PATH . LC_DS . 'apps' . LC_DS . LC_APP_ID);
define('LC_CORE_PATH', LC_ROOT_PATH . LC_DS . 'core');
define('LC_EXT_PATH', LC_CORE_PATH . LC_DS . 'extensions');
define('LC_YII_PATH', LC_ROOT_PATH . LC_DS . '..' . LC_DS . '..' . LC_DS . 'yii' . LC_DS . 'yii' . LC_DS . 'framework');

/****** DEBUG **********/
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 5);
defined('YII_DEBUG_SHOW_PROFILER') or define('YII_DEBUG_SHOW_PROFILER', YII_DEBUG);
defined('YII_DEBUG_PROFILING') or define('YII_DEBUG_PROFILING', YII_DEBUG);
defined('YII_DEBUG_DISPLAY_TIME') or define('YII_DEBUG_DISPLAY_TIME', YII_DEBUG);