<?php
/**
 * Created by JetBrains PhpStorm.
 * User: lance
 * Date: 22.07.13
 * Time: 0:46
 * To change this template use File | Settings | File Templates.
 */

namespace core\validators;


class Timestamp extends \CValidator
{
    public $allowEmpty=true;
    protected function validateAttribute($object, $attribute)
    {
        $timestamp = $object->$attribute;
        if ($timestamp === null && $this->allowEmpty)
            return null;
        $date = \Yii::app()->getDateFormatter()->format(\DateTime::W3C, $timestamp);
        if ($date !== null) {
            $dateValidator = new \CDateValidator();
            $dateValidator->format = \DateTime::W3C;
            $dateValidator->allowEmpty = false;
            $dateValidator->timestampAttribute = $attribute;
            $dateValidator->validate($object, $attribute);
            if ($timestamp !== $object->$attribute && !$object->hasErrors($attribute))
                $object->addError($attribute, 'Attribute {attribute} is not valid (непонятная хуйня)');
        } else
            $object->addError($attribute, 'Attribute {attribute} is null');
    }
}