<?php
return array(
    'Fields with {symbol} are required.' => 'Поля помеченные {symbol} обязательны к заполнению.',
    'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.' => 'Вы можете воспользоваться опциональными операторами (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
или <b>=</b>) для уточнения выборки',
    'Create' => 'Создать',
    'Save' => 'Сохранить',
    'Search' => 'Искать',
    'Advanced Search' => 'Расширенный поиск',
    'Are you sure you want to delete this item #{id}?' => 'Вы уверены, что хотите удалить данный элемент #{id} ?',
);