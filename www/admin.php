<?php

define('LC_APP_ID', 'admin');
require_once '../core/constants.php';

require_once(LC_YII_PATH . LC_DS . 'YiiBase.php');
require_once(LC_CORE_PATH . LC_DS . 'Yii.php');

Yii::setPathOfAlias('core', LC_CORE_PATH);
Yii::setPathOfAlias(LC_APP_ID, LC_APP_PATH);

/* @var admin\components\WebApplication $app */
$app = Yii::createApplication(
    'admin\\components\\WebApplication',
    LC_APP_PATH . LC_DS . 'config' . LC_DS . 'application.php'
);
//$session = Yii::app()->getSession();
$app->run();