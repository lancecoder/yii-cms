Lance Web Application
=====================

STRUCTURE
---------

    apps                      существующие приложения (frontend, admin, api, ...)
    	index                 Frontend приложение (app_id)
    		components        базовые компоненты приложения
    		config            инивидуальные настройки приложения
    		modules           модули (индивидуальные контроллеры и шаблоны)
    		runtime           системная папка
    	admin                 Админка (аналогично frontend)
    		...
    core                      общее для всех приложений
    	behaviors             поведения для базовых компонентов
    		model             поведения для моделей
    	components            базовые компоненты
    	config                общий конфиг
    	extensions            расширения
    	helpers               хелперы
    	messages              сообщения для компонентов (I18N)
    	modules               модули (основной файл и общие компоненты для всех приложений)
    www                       публичная папка с точками входа {app_id}.php (единственная доступная из веб)
    templates                 шаблоны для email, sms, ...


MODEL
-----

1. все модели должны быть унаследованны от core\components\ActiveRecord

2. на каждое enum поле необходимо создать в моделе следущее:
(на примере role ('user','admin','manager')

а. constants
```php
    static ROLE_USER = 'user';
    static ROLE_ADMIN = 'admin';
    static ROLE_MANAGER = 'manager';
```

b. translated roles
```php
    public static function getRoleLabels()
    {
        return array(
            self::ROLE_USER => Yii::t('UserAccount', self::ROLE_USER),
            self::ROLE_ADMMIN => Yii::t('UserAccount', self::ROLE_ADMIN),
            self::ROLE_MANAGER => Yii::t('UserAccount', self::ROLE_MANAGER),
        );
    }
```

c. translated role
```php
    public function getRole()
    {
        return static::getLabel($this, 'role')
    }
```

d. scope
```php
    public scopeRole($role)
    {
        return $this->scopeIn('role', $role);
    }
```

e. rules
```php
    'role', 'in', 'range'=>array_keys(self::getRoleLabels()), [ 'allowEmpty'=>false ]
```

MODULES
-------

1. папка для модуль должна соответствовать корню namespace
2. папка для сабмодулей - modules (default) @see admin\components\WebApplication::getModuleAliases() (иначе не возможно получить из неинициализированного модуля ->modulePath())
3. module id == folder name

стандарты пока не устаканились :( to be continued...


INSTALLATION
------------

1. Распаковываем архив
2. Устанавливаем права на запись (0777) для папок

    apps/index/runtime/
    apps/admin/runtime/
    www/assets/

3. Настраиваем сервер, так чтобы при обращении к корню сайта запросы обрабатывали следующим образом:

    /       =>  index.php
    /admin  =>  admin.php

4. Прописываем пути к фреймворку в вышеупомянутых файлах
5. Создаем пустую БД
6. Создаем файлы с настройками:

    core/config/components-local.php
    core/config/console-local.php

Содержащие следующий код:
```php
    return array(
        'components' => array(
            'db' => array(
                //тут настройки подключения к БД
            )
        )
    );
```
7. Запускаем миграции

    php core/yiic migrate up --migrationPath=user.migrations

TRANSLATES
----------

1. moduleID, controllerID, controllerID.actionID должны находиться в файле core/modules/moduleID/messages/main.php
user/account/register
user/account/login
user/profile/view
return array(
'n=null#User|#Users' => 'Пользователь',
'User' => 'Пользователь',
);

DEFAULTS
--------

    login:          lance.coder@yandex.ru
    password:       admin


REQUIREMENTS
------------

Минимальные требования

    * PHP 5.1.0 или выше
    * Apache2 или Nginx
    * Mysql
    * GD или Imageimagemagick
    * Windows или Linux


CONTACTS
--------

    skype: lance.coder
    mail: lance.coder@yandex.ru